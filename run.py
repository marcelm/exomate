#!/usr/bin/env python3
from exomate.application import app
import sys

import argparse

parser = argparse.ArgumentParser();
parser.add_argument('--port', '-p', default=8000, type=int, help="Listen Port (Default 8000)")
parser.add_argument('--public', default=False, action='store_true', help="Listen for external connections")
parser.add_argument('--nodebug', dest='debug', default=True, action='store_false', help="Disable debug messages")
args = parser.parse_args()

if args.public:
  host = '0.0.0.0'
else:
  host = '127.0.0.1'

app.run(host=host, port=args.port, debug=args.debug)
