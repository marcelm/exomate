
from __future__ import print_function, division, absolute_import
from pprint import pprint
import datetime
import json
import random
from string import ascii_letters, digits
from flask import request, redirect, url_for, abort
from werkzeug.datastructures import MultiDict
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from exomate.application import app
from exomate.models import ShortURLs
from exomate.database import get_session

session = get_session()

def shorten(prefix):
	def decorator(function):
		def add_shortener(short):
			try:
				shortener = session.query(ShortURLs).filter(ShortURLs.short == short)
				shortener_json = shortener.one().json
				shortener.update({ShortURLs.atime: datetime.datetime.now(), ShortURLs.count: ShortURLs.count + 1})
				session.commit()
				request.form = MultiDict(json.loads(shortener_json))
				request.environ['REQUEST_METHOD'] = "POST"
				return function()
			except NoResultFound:
				session.rollback()
				return redirect(url_for(function.__name__))
			except MultipleResultsFound:
				abort(500)

		add_shortener.__name__ = "add_shortener_" + prefix
		app.add_url_rule("/" + prefix + "/<short>", "add_shortener_" + prefix, add_shortener)

		def inner(*args, **kwargs):
			if request.method != "POST":
				return function(*args, **kwargs)
			else:
				json_request = json.dumps(request.form)
				shortstring = ''
				try:
					query = session.query(ShortURLs).filter(ShortURLs.prefix == prefix).filter(ShortURLs.json == json_request).one()
					shortstring = query.short
				except NoResultFound:
					while shortstring == '':
						shortstring = ''.join(random.choice(ascii_letters + digits) for x in range(8))
						if len(session.query(ShortURLs).filter(ShortURLs.prefix == prefix).filter(ShortURLs.short == shortstring).all()) > 0:
							shortstring = ''
					short = ShortURLs(short = shortstring, prefix=prefix, json=json.dumps(request.form), count = 0, ctime = datetime.datetime.now())
					session.add(short)
					session.commit()
				except MultipleResultsFound:
					abort(500)

				return redirect(url_for("add_shortener_" + prefix, short=shortstring))
		inner.__name__ = function.__name__
		return inner
	return decorator
