function show_hide_column(tablename, colno, do_show) {
    var stl;
    if (do_show) stl = ''
    else         stl = 'none';

	var tbl  = document.getElementById(tablename);
	var rows = tbl.getElementsByTagName('tr');

	var headercells = rows[0].getElementsByTagName('th');
	headercells[colno].style.display=stl;
    
	for (var row=1; row<rows.length;row++) {
		var cells = rows[row].getElementsByTagName('td');
		cells[colno].style.display=stl;
	}
}