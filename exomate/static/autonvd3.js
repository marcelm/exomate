/*

   autonvd3 is a jquery plugin to generate nvd3 charts from existing html tables.
   As such, it requires both jquery and nvd3 to be loaded.
   As of now, only histograms and linecharts are plotted.

   Usage:
      autonvd3 is controlled by "data-" arguments to tables. Each table has to
	  be in the "chart" class.

	  data-chart-ids: A comma seperated list of ids.
	  data-chart-<id>-x: The coulmn number, which denotes the X Axis.
	  data-chart-<id>-y: The coulmn number, which denotes the Y Axis.
	  data-chart-<id>-target: The id of the <div> to plot the chart. (If let 
	                          blank, the <id> is used)
	  data-chart-<id>-label: A label for the chart. (If let blank, the <id> is used)
	  dara-chart-<id>-sort: true, if the X-values should be sorted. Default is true

	  Additionally you can control the rendered output by adding "data-" 
	  arguments to the output div.

	  data-legend: true, if a legend should be displayed; false, if not.
					Default is "false"
	  data-control: true, if controls should be shown; false, if not.
					Default is "false"
	  data-type: The type of the chart.
				Current types are:
					"barchart": The default chart.
					"linechart": A linechart. You have to feed it numerical values on it's X-axis.
					"discretelinechart": A linechart for discrete values. 
						The values are sorted and tought to be each 1 apart. 
						If you map several charts to one, the labels of the first 
						chart are chosen as displayed labels

	  data-precision: The precision for y values in tooltips. Default is 0.

	  Each table can refenrence multiple charts, and multiple <div>s can be 
	  referenced by multiple tables

	  Example:
	  <div id="mychart" data-legend="true" data-precision="2"></div>
	  <div id="mychart2" data-type="linechart"></div>
	  <table class="chart" data-chart-ids="chart1, chart2"
	                       data-chart-chart1-x="2"
						   data-chart-chart1-y="3"
						   data-chart-chart1-target="mychart2"
						   data-chart-chart1-label="Supercool Chart"
						   data-chart-chart2-x="1"
						   data-chart-chart2-y="3"
						   data-chart-chart2-target="myChart">
	  ...
	  </table>
	  <table class="chart" data-chart-ids="mychart"
						   data-chart-mychart-x="1"
						   data-chart-mychart-y="2">
	  ...
	  </table>

 */


(function( $ ) {
	$.fn.getCharts = function() {
		var table = this
		var chartnames = $( table ).data("chart-ids").split(/[\s,]+/);
		var charts= []
		for(var name in chartnames) {
			var domain_col = parseInt($( table ).data("chart-" + chartnames[name] + "-x"));
			var range_col = parseInt($( table ).data("chart-" + chartnames[name] + "-y"));
			var target = $( table ).data("chart-" + chartnames[name] + "-target");
			if (!(target)) {
				target = chartnames[name];
			}
			var label = $( table ).data("chart-" + chartnames[name] + "-label")
			if(!(label)) {
				label = chartnames[name];
			}
			var prec = $( table ).data("chart-" + chartnames[name] + "-precision")
			if(!(prec)) {
				prec = 2
			}
			var sorted = $( table ).data("chart-" + chartnames[name] + "-sort")
			if(!(sorted)) {
				sorted = true
			}
			var values = [];

			if (!domain_col) {
				var histogram = d3.layout.histogram()
				var y = []
				$( table ).find( "tr" ).each(function() {
					if($( this ).children("td").length >= range_col) {
						var value = $(this).children("td:nth-child(" + range_col + ")").text();
						y.push(parseFloat(value));
					}
				});
				var hist = histogram(y);
				for (var i=0; i<hist.length; i++) {
					values.push({
						x: hist[i].x.toExponential(2),
						y: hist[i].y
					});
				}
				console.log(values);
			}
			else {
				$( table ).find( "tr" ).each(function() {
					if($( this ).children("td").length >= Math.max(domain_col, range_col)) {
						values.push({
							x: $(this).children("td:nth-child(" + domain_col + ")").text(), 
							y: parseFloat($(this).children("td:nth-child(" + range_col + ")").text())
						});
					}
				});
			}

			if (sorted) {
				values.sort(function(a,b){ 
					if(a.x < b.x)
						return -1;
					if(a.x > b.x)
						return 1;
					return 0
					});
			}

			charts.push({
				data: {key: label, values: values},
				target: target,
			});
		}
		return charts;
	};

	$( document ).ready(function() {
		var targets = [];
		var charts = [];
		$( "table.chart" ).each(function() {
			charts = charts.concat($( this ).getCharts());
			for (var chart in charts){
				if (jQuery.inArray(charts[chart].target, targets)) {
					targets.push(charts[chart].target);
				}
			}
		});

		for (var target in targets){
			var chart = $("#" + targets[target]).append("<svg id=\"svg_"+targets[target] + "\">");
			var legend = $("#" + targets[target]).data("legend");
			if (!(legend)){
				legend = false;
			}
			var controls = $("#" + targets[target]).data("controls");
			if (!(controls)){
				controls = false;
			}
			var prec = $("#" + targets[target]).data("precision");
			if (!(prec)){
				prec = "0";
			}
			var type = $("#" + targets[target]).data("type");
			if (!(type)){
				type = "barchart";
			}

			targetcharts = function() {
				var data = [];
				for (var chart in charts){
					if (charts[chart].target == targets[target]) {
							data.push(charts[chart].data);
					}
				}
				return data;
			}()

			var d3chart;
			if(type == "linechart"){
				d3chart = nv.models.lineChart()
					.showLegend(legend)
					.showYAxis(true)
					.showXAxis(true)
			} else if (type == 'discretelinechart') {
				d3chart = nv.models.lineChart()
					.showLegend(legend)
					.showYAxis(true)
					.showXAxis(true)
				var origcharts = targetcharts
				for (var chart in origcharts){
					for(var value in origcharts[chart].values){
						targetcharts[chart].values[value].label = targetcharts[chart].values[value].x
						targetcharts[chart].values[value].x = parseInt(value)
					}
				}
				d3chart.xAxis.tickFormat(function(d){
						return origcharts[0].values[d].label
					})
			} else {
				d3chart = nv.models.multiBarChart()
					.tooltips(true)
					.showLegend(legend)
					.showControls(controls);
			}
			d3chart.yAxis.tickFormat(d3.format('.0'+ prec +'f'));
			console.log(targets[target])
			d3.select('#' + targets[target] +  ' svg')
				.datum(targetcharts)
				.transition().duration(100)
				.call(d3chart);

			nv.addGraph(function() { return d3chart });

			nv.utils.windowResize(d3chart.update);
			// TODO disable since plots loose their axes
			/*
			$('<div>', {class: "text-right"}).append($('<a>', {text: "Download", class:"btn", id: "btn_" + targets[target], download: targets[target]+".svg", 
						style: "opacity: 0.1;",
						})).insertAfter("#svg_"+targets[target]);


			chart.hover(function(){
				copy_of_svg = $("#svg_" + targets[target]).clone();
				$.get("/static/nvd3/nv.d3.css", function(css){
					copy_of_svg.prepend('<style type="text/css"><![CDATA[' + css + ']]></style>');
					$("#btn_" + targets[target]).prop(
							"href", "data:image/svg+xml;charset=UTF-8," + 
									encodeURIComponent((new XMLSerializer).serializeToString(copy_of_svg[0]))
					);
					$("#btn_" + targets[target]).css("opacity", "1");
				});
			}, function(){
				$("#btn_" + targets[target]).css("opacity", "0.1");
			});*/
		}
	});
}( jQuery ));
