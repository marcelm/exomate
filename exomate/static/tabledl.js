(function( $ ) {
	console.log("tabledl loaded")
	$( document ).ready(function() {
		console.log("tabledl executed")
		$( "table.xls").not('.containsStickyHeaders').each(function() {
			console.log("tabledl table found")
			link = $('<a>',{class: "btn btn-primary", download: "table.xls", href: "#", 
				onclick: "return ExcellentExport.excel(this, '" + this.id +  "', 'Tabelle');",
				text: "Export to Excel"
			});
			link.insertAfter($(this));
		});
	});

}( jQuery ));
