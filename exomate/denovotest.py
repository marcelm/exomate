from math import log, exp, log1p, fsum
from itertools import combinations_with_replacement, product
from functools import reduce
from operator import mul

CONFUSION_MATRIX_ILLUMINA = dict(
	A=dict(C=0.58, G=0.17, T=0.25),
	C=dict(A=0.35, G=0.11, T=0.54),
	G=dict(A=0.32, C=0.05, T=0.63),
	T=dict(A=0.46, C=0.22, G=0.32)
)


def prob_miscall(base_quality):
	p = (10 ** (-base_quality / 10))
	return log(p)  # convert the phred scaled basequal to log-probability


def prob_base_given_allele(base, qual, allele):
	e = prob_miscall(qual)
	return log1p(-exp(e)) if base == allele else e + log(CONFUSION_MATRIX_ILLUMINA[allele][base])


def prob_base_given_genotype(base, qual, genotype):
	return log(fsum(exp(prob_base_given_allele(base, qual, allele)) for allele in genotype)) - log(2)


def prob_pileup_given_genotype(pileup, genotype):
	return fsum(prob_base_given_genotype(base, qual, genotype) for base, qual in pileup)


def prob_pileups_given_genotypes(pileup_a, pileup_b, genotypes):
	return log(fsum(exp(prob_pileup_given_genotype(pileup_a, genotype_a) + prob_pileup_given_genotype(pileup_b, genotype_b)) for genotype_a, genotype_b in genotypes))


def prob_alt_allelefreq(alt_allelefreq, heterozygosity=0.001, samples=2):
	"""
	Following the infinite-site neutral variation model (for two samples) as defined in [dePristo et al, Nat Genetics (2011)].
	Heterozygosity is given as defined in GATK UnifiedGenotyper documentation.
	"""
	return log(heterozygosity) - log(alt_allelefreq) if alt_allelefreq > 0 else log1p(-exp(log(heterozygosity) + log(sum(1 / i for i in range(1, 2 * samples)))))


def prob_genotypes(genotypes, alt):
	# for each genotype combination, calc alternate allele frequency and sum up
	allele_freqs = [genotype_a.count(alt) + genotype_b.count(alt) for genotype_a, genotype_b in genotypes]
	return log(fsum(exp(prob_alt_allelefreq(freq)) for freq in allele_freqs))


def prob_denovo_mutation(pileup_a, pileup_b, ref, alt):
	"""
	This test calulates the p-value for the existence of a de-novo mutation in the sample provided by pileup_b compared to pileup_a.
	Thereby it closely follows the bayesian method of dePristo et al, Nature Genetics 2011 (the GATK paper).
	
	The main idea is as follows:
	Given the two samples A and B, as well as reference and alternative allele X and Y,
	the possible genotypes are G_0 = {(XX, XX), (XY, XX), (YY, XX), (XX, XY), ...}.
	The possible genotypes denoting a de-novo mutation are G_1 = {(XX, XY), (XX, YY)}.
	The likelihood of the pileups D=(D_A, D_B) given a set of genotypes can be calculated as
	P(D | G) = \sum_{GT_A, GT_B \in G} P(D_A | GT_A) * P(D_B | GT_B)
	Above per-sample likelihoods are defined as in dePristo et al. 2011.
	Then, the probability for having no de-novo mutation between A and B (our null hypothesis) can be 
	calculated using Bayes Theorem and the law of total probability as
	P(G_0 - G_1 | D) = P(G_0 - G_1) * P(D | G_0 - G_1) / (P(D | G_0) * P(G_0))
	"""
	# all possible genotypes in a single sample (XX, XY, YY)
	single_genotypes = list(combinations_with_replacement([ref, alt], 2))
	# all combinations of genotypes in both samples ((XX, XX), (XY, XX), ...)
	all_genotypes = set(product(single_genotypes, repeat=2))
	# genotype combinations that denote a de-novo mutation in sample b ((XX, XY), (XX, YY))
	denovo_genotypes = set([((ref, ref), (ref, alt)), ((ref, ref), (alt, alt))])

	# null hypothesis: no de-novo mutation
	no_denovo_genotypes = all_genotypes - denovo_genotypes
	# use bayes theorem and total probability
	return prob_genotypes(no_denovo_genotypes, alt) + prob_pileups_given_genotypes(pileup_a, pileup_b, no_denovo_genotypes) - (prob_pileups_given_genotypes(pileup_a, pileup_b, all_genotypes) + prob_genotypes(all_genotypes, alt))


# test
#pileup_a = [("A", 30)] * 80 + [("T", 30)] * 4
#pileup_b = [("A", 30)] * 0 + [("T", 30)] * 200
#print(exp(prob_denovo_mutation(pileup_a, pileup_b, "A", "T")))
#ref = "A"
#alt = "T"
#denovo_genotypes = [((ref, ref), (ref, alt)), ((ref, ref), (alt, alt))]
#print(exp(prob_genotypes(denovo_genotypes, alt)))
