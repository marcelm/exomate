# coding: utf-8
from exomate.application import app
from .variants import VARIANT_OUT_NAMES

#none filter
filter_none = {None: ''}

@app.template_filter()
def none_dash(s):
	if s is None or s == "":
		return '&mdash;'
	return s


@app.template_filter()
def none_zero(s):
	if s is None or s == "":
		return 0
	return s


@app.template_filter()
def none_filter(s):
	if s in filter_none:
		return filter_none[s]
	return s


@app.template_filter()
def decimal(s):
	if s is None:
		return ''
	return "{:.2f}".format(s)


@app.template_filter()
def percentage(s, x=None):
	if s is None:
		return ''
	if x is not None:
		p = 100. * s / x
	else:
		p = 100. * s
	return "%.1f%%" % (p, )


@app.template_filter()
def yesno(s):
	if s is None:
		return ''
	elif s:
		return 'yes'
	else:
		return 'no'


@app.template_filter()
def mega(s):
	if s is not None:
		return '%.1f' % (s / 1E6, )
	else:
		return ''


@app.template_filter()
def giga(s):
	if s is not None:
		return '%.1f' % (s / 1E9, )
	else:
		return ''


@app.template_filter()
def slash_filter(s):
	return s.replace("/", "")


@app.template_filter()
def entity_break(s):
	return s.replace(" [", "<br>[")


@app.template_filter()
def consequence(s):
	ret = [VARIANT_OUT_NAMES[b] for b in VARIANT_OUT_NAMES if 2**b & s > 0]
	return ", ".join(ret)


@app.template_filter()
def hgvsp(s):
	# only ouput first
	sort = sorted(x.split(".", 1)[1] for x in s if s)
	if sort:
		return sort[0]
	return ""


@app.template_filter()
def too_long(s):
	if len(s) > 5:
		return s[:4] + "..."
	else:
		return s


@app.template_filter()
def datetime(value):
	return value.strftime('%d, %h %Y at %H:%M')


@app.template_filter()
def round(value):
	if not value is None:
		return "{0:.4g}".format(value)
	return ""

#GENDER = { None: '', 'm': u'♂', 'f': u'♀' }
GENDER = { None: '', 'm': 'm', 'f': 'f' }  # do not use symbols for now

@app.template_filter()
def gender(s):
	return GENDER[s]
