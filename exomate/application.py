from flask import Flask
from flask.ext.admin import Admin
from .adminviews import ModelViewExt, ModelViewExtWithDel


class ReverseProxied(object):
	'''Wrap the application in this middleware and configure the 
	front-end server to add these headers, to let you quietly bind 
	this to a URL other than / and to an HTTP scheme that is 
	different than what is used locally.

	In nginx:
	location /myprefix {
		proxy_pass http://192.168.0.1:5001;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Scheme $scheme;
		proxy_set_header X-Script-Name /myprefix;
		}

	:param app: the WSGI application
	'''
	def __init__(self, app):
		self.app = app

	def __call__(self, environ, start_response):
		script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
		if script_name:
			environ['SCRIPT_NAME'] = script_name
			path_info = environ['PATH_INFO']
			if path_info.startswith(script_name):
				environ['PATH_INFO'] = path_info[len(script_name):]

		scheme = environ.get('HTTP_X_SCHEME', '')
		if scheme:
			environ['wsgi.url_scheme'] = scheme
		return self.app(environ, start_response)


def __init__(self, app):
		self.app = app

app = Flask(__name__)
app.config['SECRET_KEY'] = "123456"
app.wsgi_app = ReverseProxied(app.wsgi_app)

admin = Admin(app, name="Exomate")

app.jinja_env.trim_blocks = True

#import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.getLogger('sqlalchemy.pool').setLevel(logging.DEBUG)
#logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)
#logging.getLogger('sqlalchemy.dialects').setLevel(logging.DEBUG)
#logging.getLogger('sqlalchemy.orm').setLevel(logging.DEBUG)


from exomate.views import *
import exomate.filters as filters
from exomate.models import Patient, Sample, Unit, Disease, SampleGroup, LogType, Log, Library, ImportStat, CaptureKit, Run
from exomate.database import get_session

try:
	from exomate import git_revision
	version = git_revision
except:
	version = 'unknown'
app.jinja_env.globals.update(version=version)

session = get_session()


admin.add_view(ModelViewExt(Patient, session,
	column_list=('accession', 'gender', 'father', 'mother', 'phenoscore', 'comment'),
	list_template = 'admin/patient.html'))
admin.add_view(ModelViewExt(Sample, session,
	list_template = 'admin/sample.html'))
admin.add_view(ModelViewExt(Library, session,
	list_template = 'admin/library.html'))
admin.add_view(ModelViewExt(Unit, session,
	list_template = 'admin/unit.html'))
admin.add_view(ModelViewExt(Disease, session))
admin.add_view(ModelViewExt(CaptureKit, session))
admin.add_view(ModelViewExt(Run, session))
admin.add_view(ModelViewExtWithDel(SampleGroup, session))
admin.add_view(ModelViewExtWithDel(Log, session))
admin.add_view(ModelViewExtWithDel(LogType, session))
admin.add_view(ModelViewExtWithDel(ImportStat, session))


if __name__ == '__main__':
	app.run(debug=True)
