# -*- coding: utf-8 -*-
"""
Helper classes and functions
"""

import itertools


def get_family_members(patient, visited=None):
	if visited is None:
		visited = set()

	if patient is not None and patient not in visited:
		visited.add(patient)
		yield patient
		for member in itertools.chain(*list(map(
			functools.partial(get_family_members, visited=visited),
			patient.children_father + patient.children_mother + [patient.father, patient.mother]))):
			yield member
