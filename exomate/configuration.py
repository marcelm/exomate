# This code should be written to be importable from Python 2 AND Python 3.
# kate: tab-indent on; indent-width 4; tab-width 4;
# coding: utf-8;
"""
Configuration file handling.
"""


import os

try:
	from configparser import ConfigParser
except ImportError:
	from ConfigParser import ConfigParser


def get_path():
	path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "exomate.conf")
	if not os.path.exists(path):
		path = os.getenv('EXOMATE_CONFIG')
	if path is None:
		XDG_CONFIG_HOME = os.environ.get("XDG_CONFIG_HOME",
			os.path.join(os.path.expanduser("~"), ".config"))
		path = os.path.join(XDG_CONFIG_HOME, "exomate.conf")
	if not os.path.exists(path):
		raise IOError("No configuration file found at {0}".format(path))
	return path


def parse():
	"""
	Read the configuration and return a populated ConfigParser object.

	Places to look for the configuration file, in order:
	* configuration file specified by the EXOMATE_CONFIG
	  environment variable.
	* $XDG_CONFIG_HOME/exomate.conf
	* ~/.config/exomate.conf

	The first file that is encountered is used.
	"""
	path = get_path()
	config = ConfigParser()
	config.read(path)
	return config
