# This code should be written to be importable from Python 2 AND Python 3.
# kate: tab-indent on; indent-width 4; tab-width 4;
# coding: utf-8;
"""
Manage connections to the database. The most important function here is get_session().
"""


from sqlalchemy import create_engine
from sqlalchemy.engine.url import make_url
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.pool import StaticPool

from exomate import models
from exomate import configuration

_engine = None


def database_path():
	"""
	Determine the database engine configuration string from the
	configuration file.
	"""
	return configuration.parse().get('database', 'path')


def database_url():
	"""Return configured URL as an sqlalchemy.engine.url.URL object"""
	return make_url(database_path())


def get_session(url=None, timeout=5):
	"""
	Create and return a new session. If url is None, the URL is
	obtained by calling database_url().

	The timeout parameter is the session timeout in minutes.
	Set to 0 or None to disable.
	"""
	global _engine
	if url is None:
		url = database_url()
	else:
		url = make_url(url)
	# TODO We use a low pool size to reduce the "idle in transaction"
	# processes. This is not the correct fix since the actual problem
	# is that connections don't get closed.
	if url.drivername == 'postgresql':
		extra_args = dict(pool_size=5, max_overflow=5)
	elif url.drivername == 'sqlite':
		# use a static pool to ensure that temporary tables
		# do not disappear after .commit()
		extra_args = dict(poolclass=StaticPool)
	else:
		extra_args = dict()
	_engine = create_engine(url, convert_unicode=True, echo=False, **extra_args)
	if timeout is not None and _engine.name == 'postgresql':
		_engine.execute("SET statement_timeout = '{0:d} min'".format(timeout))
	Session = scoped_session(
		sessionmaker(autocommit=False, autoflush=False, bind=_engine))
	models.Base.query = Session.query_property()

	return Session


def init():
	"""
	Create all tables. Existing tables are skipped.
	get_session() must have been called first.
	"""
	models.Base.metadata.create_all(bind=_engine)

	# create a default 'healthy' disease if it does not exist
	session = get_session()
	if session.query(models.Disease).filter(models.Disease.short_name=='healthy').first() is None:
		session.add(models.Disease(short_name='healthy', name='healthy'))
	session.commit()


def drop():
	"""
	Drop all tables.
	get_session() must have been called first.
	"""
	models.Base.metadata.drop_all(bind=_engine)
