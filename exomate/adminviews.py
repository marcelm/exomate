from flask_admin.contrib.sqla import ModelView

# -*- coding: utf-8 -*-
"""
Exomate admin-views.
"""
class ModelViewExt(ModelView):
	page_size = 40
	can_delete = False
		
	def __init__(self, view, session, column_list=None, list_template='admin/list_master.html', **kwargs):
		if column_list is not None:
			self.column_list = column_list
		if list_template is not None:
			self.list_template = list_template
		super(ModelViewExt, self).__init__(view, session, **kwargs)

class ModelViewExtWithDel(ModelView):
	page_size = 40
	can_delete = True
		
	def __init__(self, view, session, column_list=None, list_template='admin/list_master.html', **kwargs):
		if column_list is not None:
			self.column_list = column_list
		if list_template is not None:
			self.list_template = list_template
		super(ModelViewExtWithDel, self).__init__(view, session, **kwargs)
