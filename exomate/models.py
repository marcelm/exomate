# This code should be written to be importable from Python 2 AND Python 3.
# kate: tab-indent on; indent-width 4; tab-width 4;
# coding: utf-8;


from itertools import chain

from sqlalchemy import (Table, Column, Integer, SmallInteger, BigInteger,
	String, Boolean, Float, Date, DateTime, CHAR, Index, ForeignKey,
	UniqueConstraint, CheckConstraint)
from sqlalchemy.sql.functions import now
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class ShortURLs(Base):
	__tablename__ = 'short_urls'
	short = Column(String, primary_key=True)
	json = Column(String)
	prefix = Column(String, primary_key=True)
	count = Column(Integer)
	ctime = Column(DateTime)
	atime = Column(DateTime)

Index('idx_shorts', ShortURLs.short, ShortURLs.prefix, unique=True)


class LogType(Base):
	__tablename__ = 'log_type'
	id = Column(Integer, primary_key=True, nullable=False)
	name = Column(String, nullable=False)
	color = Column(String, nullable=False)

	def __repr__(self):
		return self.name


class Log(Base):
	__tablename__ = 'logs'
	created = Column(DateTime, primary_key=True, nullable=False, default=now())
	title = Column(String, nullable=False)
	text = Column(String, nullable=False)
	log_type_id = Column(Integer, ForeignKey('log_type.id'), index=True)

	log_type = relationship('LogType')


class AnnotationRun(Base):
	__tablename__ = 'annotation_run'
	id = Column(Integer, primary_key=True, nullable=False)
	max_variant_id = Column(Integer, ForeignKey('variants.id'))


class Disease(Base):
	__tablename__ = 'diseases'
	id = Column(Integer, primary_key=True, nullable=False)
	short_name = Column(String, unique=True)
	name = Column(String, unique=True)

	def __repr__(self):
		return self.name


class Patient(Base):
	__tablename__ = 'patients'
	id = Column(Integer, primary_key=True, nullable=False)
	accession = Column(String(length=40), nullable=False, unique=True)
	gender = Column(String(1), CheckConstraint("gender IS NULL OR gender IN ('m', 'f')"), default=None)
	mother_id = Column(Integer, ForeignKey('patients.id'), index=True)
	father_id = Column(Integer, ForeignKey('patients.id'), index=True)
	phenoscore = Column(Float(24))
	comment = Column(String)

	mother = relationship('Patient', primaryjoin='Patient.id==Patient.mother_id', remote_side='Patient.id', uselist=False, backref='children_mother')
	father = relationship('Patient', primaryjoin='Patient.id==Patient.father_id', remote_side='Patient.id', uselist=False, backref='children_father')

	def __repr__(self):
		return "Patient {0}".format(self.accession)


class Sample(Base):
	"""
	Theoretically, one sample is uniquely identified by a patient id, the
	tissue that was sampled and perhaps a date. Currently, there is no date
	column since we do not have multiple samples from the same patient and the
	same tissue.
	"""
	__tablename__ = 'samples'
	id = Column(Integer, primary_key=True, nullable=False)
	accession = Column(String(length=20), nullable=False, unique=True)
	patient_id = Column(Integer, ForeignKey('patients.id'), nullable=False, index=True)
	tissue = Column(String(length=20), nullable=False)
	disease_id = Column(Integer, ForeignKey('diseases.id'), nullable=False)

	disease = relationship('Disease', backref='samples')
	patient = relationship('Patient', backref='samples')

	@property
	def units(self):
		return list(chain.from_iterable(lib.units for lib in self.libraries))

	# units = relationship('Unit', backref='samples')

	def __repr__(self):
		return "<Sample({0})>".format(self.accession)

	def __str__(self):
		return self.accession

	def __gt__(self, sample):
		return self.accession > sample.accession


class Library(Base):
	"""
	A library is a sample that has been prepared for sequencing.
	From one sample, multiple libraries can be obtained that differ.
	Currently, they can differ only by exome capture kit.
	"""
	__tablename__ = 'libraries'
	id = Column(Integer, primary_key=True, nullable=False)
	sample_id = Column(Integer, ForeignKey('samples.id'), nullable=False)
	capturekit_id = Column(Integer, ForeignKey('capturekits.id'), nullable=False)

	sample = relationship('Sample', backref='libraries')
	capturekit = relationship('CaptureKit')

	def __repr__(self):
		return '<Library({}, {})>'.format(
			self.sample.accession, self.capturekit.short_name)

	def __str__(self):
		return '{} ({})'.format(
			self.sample.accession, self.capturekit.short_name)


class Run(Base):
	__tablename__ = 'runs'
	id = Column(Integer, primary_key=True, nullable=False)
	machine = Column(String)
	date = Column(Date)
	run_number = Column(Integer)
	flowcell = Column(String)
	comment = Column(String)
	# perhaps: center (sequencing center), platform (Illumina, SOLiD)

	@property
	def bases(self):
		"""
		Total no. of bases sequenced in this run, computed from unitstats
		for all units belonging to this run.
		"""
		return sum(unit.stats.bases for unit in self.units if unit.stats and unit.stats.bases)

	def __repr__(self):
		return '<Run({}, {})>'.format(self.machine, self.date)

	def __str__(self):
		return '{}'.format(self.date)


class Unit(Base):
	"""
	One sequenced unit. One unit corresponds to one (demultiplexed) FASTQ file.
	One unit can uniquely be identified by flowcell, lane and barcode.
	The most recent Illumina pipeline produces FASTQ files that contain that
	information. Since we also have some older files, for which we do not know
	flowcell and barcode, we rely on the FASTQ file name (prefix below)
	as an identifier.
	"""
	__tablename__ = 'units'
	id = Column(Integer, primary_key=True, nullable=False)
	# file name prefix
	prefix = Column(String(length=100), nullable=False, unique=True)
	library_id = Column(Integer, ForeignKey('libraries.id'), nullable=False, index=True)
	library = relationship('Library')
	lane = Column(Integer)
	barcode = Column(String)
	run_id = Column(Integer, ForeignKey('runs.id'), index=True)

	library = relationship('Library', backref='units')
	run = relationship('Run', backref='units')
	stats = relationship('UnitStats', backref='unit', uselist=False)

	def __repr__(self):
		return "<Unit {0}>".format(self.prefix)

	def __gt__(self, unit):
		return self.prefix > unit.prefix


class UnitStats(Base):
	"""Statistics"""
	__tablename__ = 'unit_stats'
	id = Column(Integer, ForeignKey('units.id'), primary_key=True, nullable=False)
	capture_coverage_1 = Column(Float(24))
	capture_coverage_5 = Column(Float(24))
	capture_coverage_10 = Column(Float(24))
	capture_coverage_20 = Column(Float(24))
	isize_mean = Column(Float(24))
	isize_stddev = Column(Float(24))
	#readcount_single = Column(Integer)  # no. of read pairs or no of reads if single-end
	bases = Column(BigInteger)  # no. of sequenced bases
	on_target = Column(BigInteger)  # no. of bases that are on the capture target
	paired = Column(Boolean)
	readlength = Column(Integer)
	readcount = Column(Integer)  # total no. of reads
	mapped = Column(Integer)  # no. of mapped reads
	duplicates = Column(Integer)  # no. of mapped reads that are PCR duplicates
	imported = Column(Boolean)


class MappingCount(Base):
	"""
	Number of reads mapping to a chromosome (idxstats info)
	"""
	__tablename__ = 'mapping_counts'
	unit_id = Column(Integer, ForeignKey('units.id'), primary_key=True, nullable=False)
	chrom = Column(String, primary_key=True, nullable=False)
	mapped = Column(Integer)
	unmapped = Column(Integer)
	unit = relationship('Unit', backref='mapping_counts')

	unit = relationship('Unit')

	def __init__(self, unit_id, chrom, mapped, unmapped):
		self.unit_id = unit_id
		self.chrom = chrom
		self.mapped = mapped
		self.unmapped = unmapped

	def __repr__(self):
		return "MappingCount(unit={0}, chrom={1}, mapped={2}, unmapped={3})".\
			format(self.unit.prefix, self.chrom, self.mapped, self.unmapped)


class Call(Base):
	__tablename__ = 'calls'
	sample_id = Column(Integer, ForeignKey('samples.id'), primary_key=True, nullable=False, index=True)
	variant_id = Column(Integer, ForeignKey('variants.id'), primary_key=True, nullable=False, index=True)
	qual = Column(Float(24), index=True)
	is_heterozygous = Column(Boolean)  # hetero-/homozygous
	read_depth = Column(Integer)
	ref_depth = Column(Integer)
	alt_depth = Column(Integer)
	strand_bias = Column(Float(24), index=True)  # GATK: FS
	qual_by_depth = Column(Float(24), index=True)  # GATK: QD
	mapping_qual = Column(Float(24), index=True)  # GATK: RMSMappingQuality
	haplotype_score = Column(Float(24), index=True)  # GATK: HaplotypeScore
	mapping_qual_bias = Column(Float(24), index=True)  # GATK: MappingQualityRanksumTest
	read_pos_bias = Column(Float(24), index=True)  # GATK: ReadPosRanksumTest

	variant = relationship('Variant')
	sample = relationship('Sample')

	def __repr__(self):
		return "Call(sample='{0}', variant={1})".format(self.sample, self.variant)


class KnownSource(Base):
	__tablename__ = 'known_sources'
	id = Column(Integer, nullable=False, primary_key=True)
	name = Column(String)

	def __init__(self, name):
		self.name = name

	def __repr__(self):
		return "KnownSource('{0}')".format(self.name)


class KnownVariant(Base):
	__tablename__ = 'known'
	variant_id = Column(Integer, ForeignKey('variants.id'), nullable=False, primary_key=True)
	source_id = Column(Integer, ForeignKey('known_sources.id'), nullable=False, primary_key=True)
	rsid = Column(String)
	allelefreq = Column(Float(24))

	# 0 - unknown, 1 - untested, 2 - non-pathogenic,
	# 3 - probable-non-pathogenic, 4 - probable-pathogenic,
	# 5 - pathogenic, 6 - drug-response, 7 - histocompatibility,
	# 255 - other
	# this field is only set to non-NULL when clinical is True.
	# (The converse is not true.)
	# could be an Enum
	clinical_significance = Column(SmallInteger)  # SCS
	clinical = Column(Boolean)  # CLN
	precious = Column(Boolean)  # PM
	# lowfreq_mutation = Column(Boolean)  # MUT -- never used in dBSNP 134
	locus_specific_db = Column(Boolean)  # LSD

	variant = relationship('Variant')
	source = relationship('KnownSource')

	def __repr__(self):
		v = self.variant
		ref = v.ref if len(v.ref) < 20 else v.ref[:17] + '...'
		alt = v.alt if len(v.alt) < 20 else v.alt[:17] + '...'
		return "KnownVariant({0}:{1} {2}->{3})".format(v.chrom, v.pos, ref, alt)


# TODO the sift_ensg table isn't needed: annotation is only based on transcripts (see sift_enst table)
class Ensg(Base):
	__tablename__ = 'sift_ensg'
	id = Column(Integer, nullable=False, primary_key=True)
	name = Column(String, index=True)
	accession = Column(String)


class Enst(Base):
	__tablename__ = 'sift_enst'
	id = Column(Integer, nullable=False, primary_key=True)
	accession = Column(String, index=True, unique=True)

class Sift(Base):
	__tablename__ = 'sift'
	chrom = Column(String, primary_key=True)
	pos = Column(Integer, primary_key=True, autoincrement=False)
	strand = Column(String(1), primary_key=True)
	enst_id = Column(Integer, ForeignKey('sift_enst.id'), primary_key=True)
	nt1 = Column(String(1))
	nt2 = Column(String(1), primary_key=True)
	ntpos1 = Column(Integer)
	ntpos2 = Column(Integer)
	score = Column(Float(24))
	median = Column(Float(24))
	seqs_rep = Column(Integer)

Index('sift_idx', Sift.chrom, Sift.pos)

class Variant(Base):
	__tablename__ = 'variants'
	id = Column(Integer, nullable=False, primary_key=True)
	chrom = Column(String, nullable=False, index=True)
	pos = Column(Integer, nullable=False, index=True)
	ref = Column(String, nullable=False, index=True)
	alt = Column(String, nullable=False, index=True)
	is_known = Column(Boolean, default=None)
	is_transition = Column(Boolean, nullable=False, index=True)
	is_transversion = Column(Boolean, nullable=False, index=True)
	healthy_count = Column(Integer, default=None, index=True)
	#context = Column(String(3), nullable=True)

	def __init__(self, chrom, pos, ref, alt):
		self.chrom = chrom
		self.pos = pos
		self.ref = ref
		self.alt = alt

	@property
	def is_indel(self):
		return not (self.is_transition or self.is_transversion)

	def __repr__(self):
		return "Variant({0}:{1} {2}->{3})".format(self.chrom, self.pos, self.ref, self.alt)

UniqueConstraint(Variant.chrom, Variant.pos, Variant.ref, Variant.alt)


class CaptureKit(Base):
	__tablename__ = 'capturekits'
	id = Column(Integer, nullable=False, primary_key=True)
	short_name = Column(String, nullable=False)
	name = Column(String, nullable=False)
	path = Column(String, nullable=True)

	def __init__(self, short_name, name, path):
		self.short_name = short_name
		self.name = name
		self.path = path

	def __repr__(self):
		return self.name


sample_group_association_table = Table('sample_group_association', Base.metadata,
	Column('sample_id', Integer, ForeignKey('samples.id')),
	Column('group_id', Integer, ForeignKey('samplegroups.id'))
)


class SampleGroup(Base):
	__tablename__ = 'samplegroups'
	id = Column(Integer, nullable=False, primary_key=True)
	name = Column(String, nullable=False)
	samples = relationship("Sample", secondary=sample_group_association_table, backref="samplegroups")


class BedAnnotation(Base):
	__tablename__ = 'bedannotations'
	accession = Column(String, nullable=False, primary_key=True)
	chrom = Column(String, nullable=False, primary_key=True)
	start = Column(Integer, nullable=False, primary_key=True)
	stop = Column(Integer, nullable=False, primary_key=True)
	name = Column(String, nullable=False, primary_key=True)
	score = Column(Float)
	strand = Column(String(1))

	def __init__(self, accession, chrom, start, stop, name, score, strand):
		self.accession = accession
		self.chrom = chrom
		self.start = start
		self.stop = stop
		self.name = name
		self.score = score
		self.strand = strand


class ImportStat(Base):
	__tablename__ = 'importstats'
	id = Column(Integer, nullable=False, primary_key=True)
	created = Column(DateTime, nullable=False, default=now())
	sample_accession = Column(String, nullable=False, unique=True)
	current_job = Column(String, nullable=False)

	def __repr__(self):
		return self.sample_accession

	def __str__(self):
		return self.sample_accession


#TODO: beta!


class Gene(Base):
	__tablename__ = 'genes'
	id = Column(Integer, primary_key=True, nullable=False)
	accession = Column(String, unique=True, nullable=True, index=True)
	name = Column(String, nullable=True)  # TODO indexed
	hi_score = Column(Float(24))    # Dataset S1 from Supplement of Huang et al., 2010 (without imputation)

	def __repr__(self):
		return "<Gene(name='{0}')>".format(self.name)


class Transcript(Base):
	__tablename__ = 'transcripts'
	id = Column(Integer, primary_key=True, nullable=False)
	accession = Column(String, unique=True, index=True)
	gene_id = Column(Integer, ForeignKey('genes.id'), nullable=True)
	biotype = Column(String)
	protein_accession = Column(String)
	feature_count = Column(Integer, nullable=True)

	gene = relationship('Gene')

	@property
	def length(self):
		return 1
#		return sum(feature.stop -feature.start for feature in self.features)

	def __repr__(self):
		return "<Transcript(name='{0}')>".format(self.name)


class Annotation(Base):
	__tablename__ = 'annotations'
	id = Column(Integer, primary_key=True, nullable=False)
	variant_id = Column(Integer, ForeignKey('variants.id'), primary_key=True, autoincrement=False, index=True)
	transcript_id = Column(Integer, ForeignKey('transcripts.id'), primary_key=True, index=True, autoincrement=False)
	gene = Column(String(15), nullable=True)
	feature = Column(String(15), nullable=True)
	feature_type = Column(String(24), nullable=True)
	consequence = Column(Integer, nullable=False)
	cdna_position_start = Column(Integer, nullable=True)
	cdna_position_stop = Column(Integer, nullable=True)
	cds_position_start = Column(Integer, nullable=True)
	cds_position_stop = Column(Integer, nullable=True)
	protein_position_start = Column(Integer, nullable=True)
	protein_position_stop = Column(Integer, nullable=True)
	ref_amino_acid = Column(String(30), nullable=True)
	alt_amino_acid = Column(String(30), nullable=True)
	ref_codon = Column(String(90), nullable=True)
	alt_codon = Column(String(90), nullable=True)
	existing_variation = Column(String(), nullable=True)
	aa_maf = Column(Float, nullable=True)
	ea_maf = Column(Float, nullable=True)
	exon_number = Column(Integer, nullable=True)
	exon_count = Column(Integer, nullable=True)
	intron_number = Column(Integer, nullable=True)
	intron_count = Column(Integer, nullable=True)
#	motif_name = Column(String(24), nullable=True)
#	motif_pos = Column(Integer, nullable=True)
#	high_inf_pos = Column(Integer, nullable=True)
	motif_score_change = Column(Float, nullable=True)
	distance = Column(Integer, nullable=True)
#	clin_sig
	canonical = Column(Boolean, nullable=True)
	symbol = Column(String(24), nullable=True)
	symbol_source = Column(String(24), nullable=True)
	sift_prediction_term = Column(String(24), nullable=True)
	sift_score = Column(Float, nullable=True)
	polyphen_prediction_term = Column(String(24), nullable=True)
	polyphen_score = Column(Float, nullable=True)
#	gmaf = 
	biotype = Column(String(50), nullable=True)
	ensp = Column(String(24), nullable=True)
	domains = Column(String(), nullable=True)
	ccds = Column(String(12), nullable=True)
	hgvsc = Column(String(), nullable=True)
	hgvsp = Column(String(), nullable=True)
	afr_maf = Column(Float, nullable=True)
	amr_maf = Column(Float, nullable=True)
	asn_maf = Column(Float, nullable=True)
	eur_maf = Column(Float, nullable=True)
#	pubmed

	transcript = relationship('Transcript')

	variant = relationship('Variant')

# export everything that's derived from "Base"
from inspect import isclass
__all__ = [ name for name, obj in list(globals().items()) if isclass(obj) and issubclass(obj, Base) ]
