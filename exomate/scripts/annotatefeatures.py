#!/usr/bin/env python3
"""
Annotate transcript position to each feature
"""
import sys
from exomate import database
from exomate.models import *
from collections import namedtuple

__author__ = "Christopher Schroeder"


def main():
	Session = database.get_session(None)
	session = Session()
	print("connected to:", session.bind.engine)

	Row = namedtuple("Row", "id, start, stop, transcript_id")

	prev = None
	prev_transcript_pos = -1

	count = 0

	for f in map(Row._make, session.execute("SELECT id, start, stop, transcript_id FROM features WHERE type = 'CDS' ORDER BY transcript_id, exon_number")):
		if count % 100000 == 0:
			print(count, "features annotated", file=sys.stderr)
		count+=1

		if prev and prev.transcript_id == f.transcript_id:
			# resuming transcript
			transcript_pos = prev_transcript_pos + prev.stop - prev.start
		else:
			# new transcript
			transcript_pos = 0

		session.execute("UPDATE features SET transcript_pos=%s WHERE id=%s" % (transcript_pos, f.id))

		prev = f 
		prev_transcript_pos = transcript_pos

	session.commit()
	session.close()


#	for i in session.query(Feature).all():
#		print(i.id)



if __name__ == '__main__':
	main()
