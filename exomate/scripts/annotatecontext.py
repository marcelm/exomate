#!/usr/bin/env python3
# kate: syntax python; remove-trailing-space-save off; remove-trailing-space off;
"""
add context to each annotation
"""
from exomate import database
from sqt.io.fasta import IndexedFasta
from exomate.commandline import HelpfulArgumentParser

__author__ = "Christopher Schröder, Sebastian Venier"

def fastq_sequence(indexedfasta, chrom, start, stop):
	return indexedfasta.get(chrom)[start:stop].decode()


def fetch_chromosomes(session):
	print("Fetching list of chromosome names ...")
	result = session.execute("SELECT DISTINCT chrom FROM variants")
	return [row.chrom for row in result]


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("fasta", metavar="FASTA",
		help="the reference in FASTA format, indexed by samtools faidx")
	args = parser.parse_args()

	Session = database.get_session(None)
	session = Session()

	indexedfasta = IndexedFasta(args.fasta)

	for chrom in fetch_chromosomes(session):
		print("chromosome", chrom)
		variants = session.execute("SELECT v.id, v.chrom, v.pos FROM variants v WHERE LENGTH(v.ref)=1 AND LENGTH(v.alt)=1 AND chrom=:chrom", {"chrom":chrom})
		for l, v in enumerate(variants):
			if l % 10000 == 0:
				print(l, "variants updated")
			context = fastq_sequence(indexedfasta, v.chrom,v.pos-1, v.pos+2)
			session.execute("UPDATE variants SET context=:context WHERE id=:variant_id", {"context":context, "variant_id":v.id})
	session.commit()


if __name__ == '__main__':
	main()
