#!/usr/bin/env python3
import exomate.database
import psycopg2
from exomate.commandline import HelpfulArgumentParser
from sqlalchemy.engine.url import make_url


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("--url", "-u", default=None,
		help="Database configuration URL. If this is missing, the configuration is read from the exomate configuration file")
	parser.add_argument("names", metavar="PATIENTNAMES", help="The Rename Patient File.")

	args = parser.parse_args()

	if args.url is None:
		url = exomate.database.database_url()
	else:
		url = make_url(args.url)
	if url.drivername != 'postgresql':
		print("Sorry, the configured database driver is {}, but only postgresql is supported by this script".format(url.drivername))
		sys.exit(1)

	connect_params = {
		"user": url.username,
		"password": url.password,
		"host": url.host,
		"database": url.database
	}
	connect_params = { k:v for k,v in list(connect_params.items()) if v != '' and v is not None }
	print("connecting to database '{database}', user '{user}' on host '{host}'".format(
		database=url.database, user=url.username, host=url.host))
	connection = psycopg2.connect(**connect_params)
	cursor = connection.cursor()

	for line in file(args.names):
		newId, sample = line.split()
		if not sample[0:1] == "M":
			sample = "M" + sample
		print(newId, sample)
		cursor.execute("UPDATE patients set accession = %s FROM samples WHERE samples.patient_id = patients.id AND samples.accession = %s", ("P" + newId, sample))

	connection.commit()
	connection.close()


if __name__ == '__main__':
	main()
