#!/usr/bin/env python3
"""
Print out information about the exomate database without needing to
start the web frontend.
"""
import sys

from sqlalchemy import func
from exomate.commandline import HelpfulArgumentParser
from exomate.database import get_session
from exomate.models import *


__author__ = "Marcel Martin"

def print_info(session):
	sq = session.query
	print("Units:", sq(Unit).count())
	print("Libraries:", sq(Library).count())
	print("Samples:", sq(Sample).count())
	print("Patients:", sq(Patient).count())
	print("Runs:", sq(Run).count())
	max_id = sq(func.max(Variant.id)).scalar()
	max_annotated = sq(func.max(AnnotationRun.max_variant_id)).scalar()
	print("Largest variant id:", max_id)
	ok = "(OK)" if max_id == max_annotated else "(annotations missing)"
	print(" Largest annotated:", max_annotated, ok)


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	args = parser.parse_args()

	session = get_session()
	print_info(session)


if __name__ == '__main__':
	main()
