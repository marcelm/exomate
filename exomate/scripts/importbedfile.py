#!/usr/bin/env python3
"""
Imports a given bedfile into database
"""
import csv
import ntpath
from exomate.commandline import HelpfulArgumentParser
from exomate.database import get_session
from exomate.models import BedAnnotation

def extract_file(path):
	head, tail = ntpath.split(path)
	return tail or ntpath.basename(head)

def importbed(path, session, zerobased=True):
	path = path
	delimiter = '\t'
	fieldnames = ["chrom", "start", "stop", "name", "score", "strand",6,7,8]

	with open(path) as csv_fp:
		csv_reader = csv.DictReader(csv_fp, delimiter=delimiter, fieldnames=fieldnames)
		wrong_strand = []
		not_enough_columns = []

		current_row = 0
		for row in csv_reader:
			strand = None
			current_row += 1
			chrom = row["chrom"]

			if chrom.startswith('chr'):
				chrom = chrom[3:]

			if not row["name"]:
				not_enough_columns.append(current_row)
				continue

			if row["strand"] == '.':
				strand = None
			elif row["strand"] in '+-':
				strand = row["strand"]
			else:
				wrong_strand.append(current_row)
				continue

			entry = BedAnnotation(extract_file(path), chrom, int(row["start"])+zerobased, int(row["stop"])-1+zerobased, row["name"], row["score"], strand)
			session.add(entry)

		if wrong_strand:
			print("Following rows have bad strand entrys and are not stored into the database:")
			for row in wrong_strand:
				print(row)

		if not_enough_columns:
			print("Following rows have not enough columns and are not stored into the database:")
			for row in not_enough_columns:
				print(row)


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("bedfile", help="CSV file with Data to import")
	parser.add_argument("--one-based", action="store_true", help="Expect one-based intervals")
	args = parser.parse_args()

	session = get_session()

	importbed(args.bedfile, session, zerobased=not args.one_based)

	session.commit()
	session.close()


if __name__ == '__main__':
	main()
