#!/usr/bin/env python3
"""
Import meta information into database.
Expected columns: date, sample, linked, father, mother, lane, gender, tissue, barcode, disease, capture-kit, comment
"""

# TODO: check if the sample already exists and only add a new library

import sys, csv
import datetime
import re
from exomate.database import get_session
from exomate.models import Base, Patient, Sample, Unit, Run, CaptureKit, Disease, Library
from exomate.commandline import HelpfulArgumentParser


anonymous_patients = set()

def without_empty(d):
	return { key: value for (key, value) in list(d.items()) if value is not None and value != '' }


def splitID(sid, sample_re, family_re):
#	import pdb; pdb.set_trace()
	# extract the sampleid (ssssss) in xxx-xxxxx-ssssss
	if sid == None:
		return (None, None)

	#extract sample id
	match     = re.search(sample_re, sid)
	if match is None: return (None, None)
	sample_id = match.group(1)

	#extract family id
	match     = re.search(family_re, sid)
	if match is None: return (None, sample_id)

	family_id = match.group(1)

	return (family_id, sample_id)


def is_anonymous(pid):
	return re.match("u[0-9]*",pid)


def kill_suffix(s):
	if s[-1:] in ['I', 'M', 'V', 'G']:
		return s[:-1]
	return s


def create_new_anonymous_patient(session, pid, gender):
	newPatient = Patient(accession=pid, gender=gender)
	anonymous_patients.add(newPatient)
	session.add(newPatient)
	session.flush()

def import_units(session, path, sample_re, family_re, dry_run, sample_prefix, patient_prefix):
	with open(path) as f:
		lines = f.readlines()
	records = list(csv.DictReader(lines, delimiter='\t'))
	records = [ without_empty(record) for record in records ]

	next_round = True

	not_inserted = set()

	while next_round == True:
		next_round = False
		print("\nnext round")
		later_use_records = []
		for record in records:
			familyname, samplename = splitID(record['sample'], sample_re, family_re)
			samplename = kill_suffix(samplename)
			patientname = record.get('patient', "".join([patient_prefix, samplename]))

			sample = session.query(Sample).filter(Sample.accession == ''.join([sample_prefix, samplename])).first()
			capturekit = session.query(CaptureKit).filter(CaptureKit.short_name == record['capture-kit']).first()
			disease = session.query(Disease).filter(Disease.short_name == record.get('disease', 'healthy')).first()
			run = session.query(Run).filter(Run.date == record['date']).first()
			unit_prefix = record.get('unit', ''.join([sample_prefix, samplename]))
			unit = session.query(Unit).filter(Unit.prefix == unit_prefix).first()
			
			if unit is not None:
				print("Unit", unit_prefix, "already exists.")
				continue


			if sample is not None:
				print("Sample", ''.join([sample_prefix, samplename]), "already exist")

			if run is None:
				print("Run", record['date'], "not found.")
				continue

			if capturekit is None:
				print("Capture-kit", record['capture-kit'], "not found. Please create this capture-kit.")
				continue

			if disease is None:
				print("Disease", record.get('disease', 'healthy'), "not found. Please create this disease.")
				continue

			# get patient, if there exist a linked sample, or the sample already exist
			if record.get('linked', None) is not None:
				#import pdb; pdb.set_trace()
				linkedfamilieid, linkedid = splitID(record['linked'], sample_re, family_re)
				linkedSample = session.query(Sample).filter(Sample.accession == ''.join([sample_prefix, linkedid])).first()
				#print(session.query(Sample).all())
				# maybe not created yet
				if linkedSample is None:
					print("linked sample", linkedid, "not found")
					later_use_records = later_use_records + [record]
					continue
				patient = linkedSample.patient
			else:
				# TODO sort out if this line still makes sense
				patient = session.query(Patient).filter(Patient.accession == patientname).first()

			if patient is None:
			# create a new patient, if there is none
				familyname, motherid = splitID(record.get('mother', None), sample_re, family_re)
				mother = None
				if motherid is not None:
					motherid = sample_prefix + motherid
					mother = session.query(Patient).join(Sample).filter(Sample.accession == motherid).first()
					if mother is None:
						print("no mother", motherid, "found for", samplename)
						# there is a motherid but no mother patient
						if is_anonymous(motherid):
							create_new_anonymous_patient(session, motherid, 'f')
							next_round = True
						later_use_records = later_use_records + [record]
						continue

				familyname, fatherid = splitID(record.get('father', None), sample_re, family_re)
				father = None
				if fatherid is not None:
					fatherid = sample_prefix + fatherid
					father = session.query(Patient).join(Sample).filter(Sample.accession == fatherid).first()
					if father is None:
						print("no father", fatherid, "found for", samplename)
						# there is a fatherid but no father patient
						if is_anonymous(fatherid):
							create_new_anonymous_patient(session, fatherid, 'm')
							next_round = True
						later_use_records = later_use_records + [record]
						continue

				# create the patient and add him to the session
				gender = gender=record.get('gender', '')
				if gender not in ['f', 'm']:
					gender = None

				patient = Patient(accession=patientname, mother=mother, father=father, gender=gender)
				session.add(patient)

				# maybe this patient was a parent, so try another round of insertion
				next_round = True
				print("Patient", patient.accession , "inserted")
			else:
				print('Patient', patient.accession, 'for sample', ''.join([sample_prefix, samplename]), "used")

			if sample is None:
				sample = Sample(
					accession=''.join([sample_prefix, samplename]),
					tissue = record.get('tissue', ''),
					disease = disease,
					patient = patient)
				session.add(sample)
				print("Sample", sample.accession , "inserted")

			newLibrary = Library(
				sample = sample,
				capturekit = capturekit)
			session.add(newLibrary)
			print("Library for", sample.accession , "inserted")

			# TODO: prefix not necessary right,maybe this should be and extra column in template
			newUnit = Unit(
				prefix = record.get('unit', ''.join([sample_prefix, samplename])),
				library = newLibrary,
				lane = record.get('lane', None),
				run = run,
				barcode = record.get('barcode', None)
				)
			session.add(newUnit)
			print("Unit", newUnit.prefix , "inserted")

			session.flush()
		records = later_use_records

	if not dry_run:
		session.commit()
	if len(later_use_records) > 0:
		print("following samples were not inserted, because of icorrect mother or father informations:")
		for record in later_use_records:
			print(record['sample'])
	else:
		print("all samples imported")

	if len(not_inserted) > 0:
		print("following samples were not inserted, already existed:")
		for sample in not_inserted:
			print(sample)


		#	existing = session.query(Unit).filter(Unit.prefix == record['prefix']).first()
		#	sample = session.query(Sample).filter(Sample.accession == record['sample']).first()
		#	run = session.query(Run).filter(Run.date == record['run_date']).first()
		#	if existing is None:
		#		print("adding", record['prefix'])
		#		session.add(Unit(prefix=record['prefix'], sample=sample, lane=record.get('lane', None), run=run, barcode=record.get('barcode', None)))
		#	else:
		#		print("updating", record['prefix'])
		#		existing.sample = sample
		#		existing.lane = record.get('lane', None)
		#		existing.run = run
		#		existing.barcode = record.get('barcode', None)
		#session.commit()


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("units", help="CSV file with units metadata")
	parser.add_argument("--sample_re", help="Regular Expression to extract sample id from sample field", default="(.*)")
	parser.add_argument("--family_re", help="Regular Expression to extract family id from sample field", default="(.*)")
	parser.add_argument("--dry-run", "-n", help="Dry run", action="store_true")
	parser.add_argument("--sample-prefix", help="Prefix to add to sample ids", default="")
	parser.add_argument("--patient-prefix", help="Prefix to add to patient ids", default="")
	args = parser.parse_args()

	session = get_session() #url=args.url)
	import_units(session, args.units, args.sample_re, args.family_re, args.dry_run, args.sample_prefix, args.patient_prefix)


if __name__== '__main__':
	main()
