#!/usr/bin/env python3
"""
Filter a gtf for protein_coding exons only and deleting redundant exon information.
A CRD section [a;b] in the gtf file implies a exon section [a;b], so the "exon" [a;b] can be deleted.
Exons [c;b] with corresponding CRD [a;b] or [c:d] remain untouched. The compression is important to import the gtf file to IGV on a computer with less than 2gb memory.
"""
import sys
from exomate.commandline import HelpfulArgumentParser

def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("input", metavar="INPUT",
		help="the gtf input file to filter and compress")

	parser.add_argument("output", metavar="OUTPUT",
		help="the filtered and compressed gtf output file")

	args = parser.parse_args()

	out = open(args.output, 'w+')

	lastline  = ""
	laststart = 0
	laststop = 0
	for line in open(args.input):
		linesplit = line.split("\t", 5)
		if linesplit[1] == "protein_coding":
			if linesplit[2] == "exon":
				lastline = line
				laststart = linesplit[3]
				laststop = linesplit[4]
			else:

				if linesplit[3] != laststart or linesplit[4] != laststop and len(lastline) > 0:
					#print(linesplit[3], laststart, linesplit[4], laststop)
					out.write(lastline)
					lastline = ""
				out.write(line)
	out.close()

if __name__ == '__main__':
	main()
