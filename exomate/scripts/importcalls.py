#!/usr/bin/env python3
# kate: syntax Python; tab-indent on; indent-width 4;
"""
Import called variants into exomate database.

Expects VCF files created by GATK.

The script needs the FASTA reference since it normalizes indels.

This is an example of two entries observed within a single VCF file that
are actually equivalent:

3       193090101       rs57458521      A       AGG     2154.44
3       193090105       .       G       GGG     2328.71

Normalization makes those entries equal.
"""
import sys
import os.path
import time
from collections import namedtuple
from itertools import islice
import psycopg2
import vcf
from sqlalchemy.engine.url import make_url
from exomate.commandline import HelpfulArgumentParser
from sqt.io.xopen import xopen
from sqt.io.fasta import IndexedFasta
import exomate.database
from exomate.variants import normalized_indel, transition_transversion

__author__ = "Marcel Martin"


Entry = namedtuple("Entry",
	"is_heterozygous read_depth ref_depth alt_depth strand_bias qual_by_depth mapping_qual haplotype_score mapping_qual_bias read_pos_bias")


def snp_record_to_entry(record, ref_depth, alt_depth):
	"""
	Convert a VCF record to a tuple suitable for insertion into the database
	"""
	read_depth = record.INFO['DP']
	is_heterozygous = record.INFO['AC'][0] == 1
	strand_bias = record.INFO['FS']
	get = record.INFO.get
	haplotype_score = get('HaplotypeScore', None)
	mapping_qual = get('MQ', None)
	qual_by_depth = get('QD', None)
	mapping_qual_bias = get('MQRankSum', None)
	read_pos_bias = get('ReadPosRankSum', None)

	return Entry(is_heterozygous, read_depth, ref_depth, alt_depth,
		strand_bias, qual_by_depth, mapping_qual, haplotype_score,
		mapping_qual_bias, read_pos_bias)


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("--limit", type=int, default=None, metavar="N",
		help="Import only the first N variants of each given VCF file (default: no limit)")
	parser.add_argument("--filter", default=set(["PASS"]), nargs="+", type=set,
		help="Import only calls whose filter column is a subset of the given values.")
	parser.add_argument("--dryrun", "-n", action="store_true")
	parser.add_argument("--clear-existing", "-c", action='store_true', default=False,
		help="Clear database records and re-import when a sample is already in the database (default: %(default)s)")
	parser.add_argument("--fail-if-exists", action='store_true', default=False,
		help="Fail if the sample is already in the database")
	parser.add_argument("--url", "-u", default=None,
		help="Database configuration URL. If this is missing, the configuration is read from the exomate configuration file")
	parser.add_argument("fasta", metavar="FASTAREF",
		help="Reference sequence in FASTA format. Must have been indexed by samtools faidx")
	parser.add_argument("vcf", nargs='+', metavar='VCF', help="name of a VCF file")
	args = parser.parse_args()

	if args.url is None:
		url = exomate.database.database_url()
	else:
		url = make_url(args.url)
	if url.drivername != 'postgresql':
		print("Sorry, the configured database driver is {}, but only postgresql is supported by this script".format(url.drivername))
		sys.exit(1)

	# set up connection arguments
	connect_params = {
		"user": url.username,
		"password": url.password,
		"host": url.host,
		"database": url.database
	}
	connect_params = { k:v for k,v in connect_params.items() if v != '' and v is not None }
	print("connecting to database '{database}', user '{user}' on host '{host}'".format(
		database=url.database, user=url.username, host=url.host))
	connection = psycopg2.connect(**connect_params)
	cursor = connection.cursor()

	fastaref = IndexedFasta(args.fasta)
	cursor.execute("""
	CREATE TEMPORARY TABLE vcf (
		chrom VARCHAR NOT NULL,
		pos INTEGER NOT NULL,
		ref VARCHAR NOT NULL,
		alt VARCHAR NOT NULL,
		qual FLOAT(24),
		is_heterozygous BOOLEAN,
		read_depth INTEGER,
		ref_depth INTEGER,
		alt_depth INTEGER,
		strand_bias FLOAT(24),
		qual_by_depth FLOAT(24),
		mapping_qual FLOAT(24),
		haplotype_score FLOAT(24),
		mapping_qual_bias FLOAT(24),
		read_pos_bias FLOAT(24),
		is_transition BOOLEAN,
		is_transversion BOOLEAN
	)""")
	cursor.execute("CREATE INDEX vcf_idx ON vcf (chrom, pos, ref, alt)")
	n_imported_files = 0
	for vcfname in args.vcf:
		print("working on", vcfname)
		sys.stdout.flush()
		reader = vcf.Reader(filename=vcfname)
		if len(reader.samples) > 1:
			raise NotImplementedError("reading multi-sample VCFs not supported")
		rg = reader.samples[0]
		cursor.execute("SELECT id FROM samples WHERE accession = %s", (rg, ))
		row = cursor.fetchone()
		if row is None:
			print('Sample "{}" was not found in the "samples" table.'.format(rg),
				file=sys.stderr)
			sys.exit(1)
		sample_id = row[0]
		cursor.execute("SELECT EXISTS (SELECT * FROM calls WHERE sample_id = %s)", (sample_id, ))
		exists = cursor.fetchone()[0]
		if exists:
			if args.clear_existing:
				if not args.dryrun:
					cursor.execute("SELECT count(*) FROM calls WHERE sample_id = %s", (sample_id, ))
					n_calls = cursor.fetchone()[0]
					print(' ', rg, "is already in the database, clearing", n_calls, "records ...", end="")
					sys.stdout.flush()
					cursor.execute("DELETE FROM calls WHERE sample_id = %s", (sample_id, ))
					print(" done")
			else:
				if args.fail_if_exists:
					print('error:', rg, 'is already in the database.')
					sys.exit(1)
				else:
					print(rg, 'is already in the database, skipping it.')
					continue
		print("  parsing VCF and importing into temporary table ...")

		start_time = time.time()  # wall-clock time
		n_imported = 0
		n_skipped = 0
		for record in islice(reader, 0, args.limit):
			filter = set(record.FILTER) if record.FILTER else set()
			if not filter <= args.filter:
				n_skipped += 1
				continue

			samplerecord = record.samples[0]
			
			try:
				#  TODO at the moment, GATK sometimes fails to recover the AD when splitting a vcf with combined calls
				ref_depth = samplerecord['AD'][0]
				alt_depths = samplerecord['AD'][1:]
			except AttributeError:
				ref_depth = None
				alt_depths = [None] * len(record.ALT)

			for alt, alt_depth in zip(record.ALT, alt_depths):
				alt = str(alt)
				if record.CHROM in fastaref:
					refseq = fastaref.get(record.CHROM)

					norm_pos, norm_ref, norm_alt = normalized_indel(
						refseq, record.POS - 1, bytes(record.REF, 'ascii'), bytes(alt, 'ascii'))
					norm_ref = norm_ref.decode('ascii')
					norm_alt = norm_alt.decode('ascii')
				else:
					norm_pos, norm_ref, norm_alt = record.POS - 1, bytes(record.REF, 'ascii'), bytes(alt, 'ascii')

				is_transition, is_transversion = transition_transversion(norm_ref, norm_alt)

				entry = snp_record_to_entry(record, ref_depth, alt_depth)

				filtered_columns = []
				filtered_entry = []
				for name, value in zip(entry._fields, entry):
					if value is not None:
						filtered_columns.append(name)
						filtered_entry.append(value)
				values = tuple([record.CHROM, norm_pos, norm_ref, norm_alt, record.QUAL] + filtered_entry + [is_transition, is_transversion])
				cursor.execute('''INSERT INTO vcf (chrom, pos, ref, alt, qual,
					{}, is_transition, is_transversion) VALUES ({})'''.format(','.join(filtered_columns), ','.join(['%s'] * len(values))),
					values)
				n_imported += 1
		parse_time = time.time()
		print("  parsed {} calls in {:.2f} minutes. {:.2f} ms per call".format(
			n_imported, (parse_time - start_time) / 60, (parse_time - start_time) / n_imported * 1000))
		print("  skipped {} calls due to FILTER column".format(n_skipped))
		print("  acquiring lock ...")
		cursor.execute("LOCK TABLE variants")
		print("  create entries in variants table ...")
		cursor.execute('''
			INSERT INTO variants (chrom, pos, ref, alt, is_transition, is_transversion)
			SELECT DISTINCT vcf.chrom, vcf.pos, vcf.ref, vcf.alt, vcf.is_transition, vcf.is_transversion FROM vcf LEFT JOIN variants ON
					variants.chrom = vcf.chrom AND
					variants.pos = vcf.pos AND
					variants.ref = vcf.ref AND
					variants.alt = vcf.alt
					WHERE variants.id IS NULL''')
		# release lock
		connection.commit()
		cursor = connection.cursor()
		variants_time = time.time()
		print("  done in {:.2f} minutes".format((variants_time - parse_time) / 60))
		if not args.dryrun:
			print("  insert into calls table")
			cursor.execute('''
				INSERT INTO calls
				(sample_id, variant_id, qual, is_heterozygous, read_depth, ref_depth, alt_depth, strand_bias, qual_by_depth, mapping_qual, haplotype_score, mapping_qual_bias, read_pos_bias)
				SELECT DISTINCT ON (vid) %s,
					(SELECT id FROM variants
						WHERE
						variants.chrom = vcf.chrom AND
						variants.pos = vcf.pos AND
						variants.ref = vcf.ref AND
						variants.alt = vcf.alt
					) vid,
					qual, is_heterozygous, read_depth, ref_depth, alt_depth, strand_bias, qual_by_depth, mapping_qual, haplotype_score, mapping_qual_bias, read_pos_bias
				FROM vcf
				''', (sample_id,))

			end_time = time.time()
			print("  done in {:.2f} minutes".format((end_time - variants_time) / 60))
		print('  clearing temporary table ...')
		cursor.execute('DELETE FROM vcf')
		diff = end_time - start_time
		# TODO cursor.execute("UPDATE unit_stats SET imported=TRUE WHERE id=%s", (unit_id,))
		# TODO cursor.execute(
		#	"INSERT INTO unit_stats (id, imported) "
		#	"SELECT %s, TRUE WHERE NOT EXISTS (SELECT 1 FROM unit_stats WHERE id=%s)",
		#	(unit_id, unit_id))
		print(' {} calls imported in {:.2f} minutes. {:.2f} ms per call'.format(
			n_imported, diff / 60, diff / n_imported * 1000))
		connection.commit()
		n_imported_files += 1

	print("finished importing.")

	connection.commit()
	connection.close()


if __name__ == '__main__':
	main()
