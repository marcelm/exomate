#!/usr/bin/env python3
"""
Import ENSEMBL gene annotations into the database.

TODO this is slow. It used to take 10 minutes with SQLite, now 84 minutes.
note: parsing takes less than 0.1 ms per feature, the rest is SQLalchemy/Postgres
"""
import sys
import time
from exomate.gtf import parse_gtf
from exomate.commandline import HelpfulArgumentParser
from exomate.database import get_session

__author__ = "Marcel Martin"

session = get_session()

def import_gtf(filename):
	"""
	"""
	n = 0
	start_time = time.time() # wall-clock time!

	for record in parse_gtf(filename):
		if 'transcript_id' not in record.attributes:
			continue
		# insert gene
		session.execute("INSERT INTO genes (accession, name) SELECT :gene_id, :gene_name "
			" WHERE NOT EXISTS (SELECT id FROM genes WHERE accession = :gene_id)",
			record.attributes)
		gene_id = session.execute("SELECT id FROM genes WHERE accession = :gene_id", record.attributes).fetchone()[0]

		# insert transcript
		values = dict(
			transcript_id=record.attributes['transcript_id'],
			transcript_name=record.attributes['transcript_name'],
			protein_accession=record.attributes.get('protein_id', None),
			gene_id=gene_id,
			strand=record.strand,
			source=record.source,
			chrom=record.chrom
		)
		session.execute("INSERT INTO transcripts (accession, name, gene_id, protein_accession, strand, source, chrom) "
			" SELECT :transcript_id, :transcript_name, :gene_id, :protein_accession, :strand, :source, :chrom"
			" WHERE NOT EXISTS (SELECT id FROM transcripts WHERE accession = :transcript_id)", values)
		transcript_id = session.execute("SELECT id FROM transcripts WHERE accession = :transcript_id",
			record.attributes).fetchone()[0]

		if record.feature == 'CDS' and 'protein_id' in record.attributes:
			session.execute("UPDATE transcripts SET protein_accession = :v WHERE id = :transcript_id",
				dict(v=record.attributes['protein_id'], transcript_id=transcript_id))

		# insert feature
		values = dict(
			start=record.start,
			end=record.stop,
			frame=record.frame if record.frame in [0,1,2] else None,
			ftype=record.feature,
			transcript_id=transcript_id,
			exon_number=record.attributes.get('exon_number', None)
		)
		session.execute("INSERT INTO features (start, stop, frame, type, transcript_id, exon_number) "
				"VALUES (:start, :end, :frame, :ftype, :transcript_id, :exon_number)",
			values)

		n += 1
		if n % 10000 == 0:
			t = time.time() - start_time
			print(n, "features imported in {0:.1f} min. {1:.2f} ms per feature.".format(t / 60.0, 1000.0 * t / n))
	session.commit()
	t = time.time() - start_time
	print(n, "features imported in {0:.1f} min. {1:.2f} ms per feature.".format(t / 60.0, 1000.0 * t / n))
	print("totals:")
	for tab in ("features", "transcripts", "genes"):
		print(tab, ": ", session.execute("SELECT count(*) FROM {0}".format(tab)).fetchone()[0], sep='')


def main():
	parser = HelpfulArgumentParser(usage=__doc__)
	parser.add_argument("gtf", metavar="GTF", help="Name of GTF file (map be gzipped)")
	args = parser.parse_args()

	import_gtf(args.gtf)
	session.remove()


if __name__ == '__main__':
	main()
