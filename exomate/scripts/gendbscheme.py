import sadisplay
import argparse
import tempfile
import subprocess
from exomate import models

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('--type', '-T', default='svg', type=str, help="Output format of 'dot'")
	parser.add_argument('file', type=str, help="Output file")
	args = parser.parse_args()

	desc = sadisplay.describe([getattr(models, attr) for attr in dir(models)])

	with tempfile.NamedTemporaryFile(mode='w') as tmpfile, open(args.file, 'wb') as outputfile:
		tmpfile.write(sadisplay.dot(desc))
		subprocess.call(['dot', '-T%s' % args.type, tmpfile.name], stdout=outputfile)
