#!/usr/bin/env python3
"""
Export all calls of healthy samples in vcf format
"""

import exomate.database
from exomate.database import get_session
from exomate.models import Call, Sample, Disease, Variant
from sqlalchemy import distinct, func, or_
from exomate.commandline import HelpfulArgumentParser

def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("--occurrence", type=int, default=1, help='the minimum count of samples having a variant')
	parser.add_argument("-t", nargs='*', help='also extract from samples with these tissues')
	args = parser.parse_args()

	conditions = [Disease.name=="healthy"]
	if args.t:
		conditions.append(Sample.tissue.in_(args.t))

	session = get_session()
	variants = session.query(Variant).join(Call).join(Sample).join(Disease).group_by(Variant).having(func.count(Call.sample_id) >= args.occurrence).filter(or_(*conditions)).all()

	print("CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER" ,"INFO")
	for v in variants:
		print(v.chrom, v.pos+1, "", v.ref, v.alt, ".", ".", ".", sep="\t")

if __name__ == '__main__':
	main()
