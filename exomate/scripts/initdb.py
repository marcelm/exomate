#!/usr/bin/env python3
from argparse import ArgumentParser
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine


def main():
	parser = ArgumentParser(description=__doc__)
	parser.add_argument('-n', '--dry-run', action='store_true', default=False,
		help='do not initialize table, show table creation statements only')
	parser.add_argument('--drop', action='store_true', default=False,
		help='Remove all tables, then re-create them')
	parser.add_argument('--url', default=None)
	args = parser.parse_args()

	if args.dry_run:
		def dump(sql, *multiparams, **params):
			print(sql.compile(dialect=engine.dialect))
		engine = create_engine('postgresql://', strategy='mock', executor=dump)
		db_session = scoped_session(
			sessionmaker(autocommit=False, autoflush=False, bind=engine))
		from exomate import models
		models.Base.query = db_session.query_property()
		models.Base.metadata.create_all(bind=engine)
	else:
		from exomate import database
		# The returned session is not used, but get_session must have
		# been called before database.init() can be called.
		_ = database.get_session(args.url)
		if args.drop:
			database.drop()
		database.init()


if __name__ == '__main__':
	main()
