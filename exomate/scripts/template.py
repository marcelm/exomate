#!/usr/bin/env python3
"""
Foo a bar, using a specific baz while doing so. The result is written to standard output.

Example:

%(prog)s something1 somewhere2
"""
import sys

from exomate.commandline import HelpfulArgumentParser

__author__ = ""


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("--histogram", default=None,
		help="Write a read length histogram in PostScript format to this file")
	parser.add_argument("-s", "--summarize", action="store_true", dest="summary", default=False,
		help="Print summary (default: %(default)s)")
	parser.add_argument("-c", "--colorspace", action='store_true', default=False,
		help="Work in color space")
	parser.add_argument("bam", metavar='BAM', help="output BAM file")
	parser.add_argument("fastq", metavar='FASTQ', help="input file in FASTQ format")
	args = parser.parse_args()

	if False:
		print("there was an error!", file=sys.stderr)
		sys.exit(1)

	print("file names:", args.bam, args.fastq)
	print("Write histogram to this file:", args.histogram)
	print("Work in color space:", args.colorspace)


if __name__ == '__main__':
	main()
