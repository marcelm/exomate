#!/usr/bin/env python3
"""
Import one or more VCF files into the table of known variants within the database.
"""
import sys
import time
import os.path
from itertools import islice
from vcf import Reader as VcfReader
from sqlalchemy.engine.url import make_url
from sqlalchemy import (Table, Column, Integer, SmallInteger, BigInteger,
	String, Boolean, Float, Date, DateTime, CHAR, Index, ForeignKey,
	UniqueConstraint, CheckConstraint)
from sqt.io.fasta import IndexedFasta
from exomate import database
from exomate.variants import normalized_indel, transition_transversion
from exomate.models import Base, Variant, KnownVariant, KnownSource
from exomate.commandline import HelpfulArgumentParser

__author__ = "Marcel Martin"


"""
III. Darf nur bestimmte Werte enthalten:
SCS = 0, 1, 2, 6, 7

IV. Dürfen nicht vorhanden sein (d.h. SNPs mit diesen Flags werden nicht rausgefiltert):
CLN, PM, MUT, CDA, LSD, WTD
ggf. einstellbar: NSF, NSM, NSN, ASS, DSS
"""

CLINICAL_SIGNIFICANCE = {
	"unknown": 0,
	"untested": 1,
	"non-pathogenic": 2,
	"probable-non-pathogenic": 3,
	"probable-pathogenic": 4,
	"pathogenic": 5,
	"drug-response": 6,
	"histocompatibility": 7,
	"other": 255
}


class Vcf(Base):
	__tablename__ = 'vcf_temp'
	__table_args__ = dict(prefixes=['TEMPORARY'])
	chrom = Column(String, nullable=False, primary_key=True)
	pos = Column(Integer, nullable=False, primary_key=True)
	ref = Column(String, nullable=False, primary_key=True)
	alt = Column(String, nullable=False, primary_key=True)
	rsid = Column(String, nullable=True) # some variants differ only by rsid
	allelefreq = Column(Float(24))
	is_transition = Column(Boolean)
	is_transversion = Column(Boolean)

	# 0 - unknown, 1 - untested, 2 - non-pathogenic,
	# 3 - probable-non-pathogenic, 4 - probable-pathogenic,
	# 5 - pathogenic, 6 - drug-response, 7 - histocompatibility,
	# 255 - other
	# this field is only set to non-NULL when clinical is True.
	# (The converse is not true.)
	# could be an Enum
	clinical_significance = Column(SmallInteger) # SCS
	clinical = Column(Boolean) # CLN
	precious = Column(Boolean) # PM
	locus_specific_db = Column(Boolean) # LSD

	def __init__(self, chrom, pos, ref, alt, rsid, allelefreq,
			clinical_significance, clinical, precious, locus_specific_db,
			is_transition, is_transversion):
		self.chrom = chrom
		self.pos = pos
		self.ref = ref.decode("ascii")
		self.alt = alt.decode("ascii")
		self.rsid = rsid
		self.allelefreq = allelefreq
		self.clinical_significance = clinical_significance
		self.clinical = clinical
		self.precious = precious
		self.locus_specific_db = locus_specific_db
		self.is_transition = is_transition
		self.is_transversion = is_transversion


class Importer:
	def __init__(self, session, source_id, block_size):
		self.session = session
		self.source_id = source_id
		self.n = 0
		self.block_size = block_size

		#self.vcf_insert = Vcf.__table__.insert()
		#self.vcf_insert.bind = session.connection()

	def commit(self):
		"""move all records accumulated so far from the temporary table into the other tables"""
		#self.session.execute("CREATE INDEX vcf_temp_idx ON vcf_temp (chrom, pos, ref, alt)")
		self.session.flush()
		self.session.execute('''
			INSERT INTO variants (chrom, pos, ref, alt, is_transition, is_transversion)
			SELECT DISTINCT vcf_temp.chrom,
					vcf_temp.pos,
					vcf_temp.ref,
					vcf_temp.alt,
					vcf_temp.is_transition,
					vcf_temp.is_transversion
					FROM vcf_temp LEFT JOIN variants ON
					variants.chrom = vcf_temp.chrom AND
					variants.pos = vcf_temp.pos AND
					variants.ref = vcf_temp.ref AND
					variants.alt = vcf_temp.alt
					WHERE variants.id IS NULL''')
		# dbSNP VCFs are strange: there are duplicate entries (same variant, different rsid),
		# making the DISTINCT necessary
		self.session.execute('''
			INSERT INTO known
			(variant_id, source_id,                 rsid, allelefreq, clinical_significance, clinical, precious, locus_specific_db)
			SELECT DISTINCT ON (vid, sid) vid, sid, rsid, allelefreq, clinical_significance, clinical, precious, locus_specific_db
			FROM (
				SELECT (
					SELECT id FROM variants WHERE
					variants.chrom = vcf_temp.chrom AND
					variants.pos = vcf_temp.pos AND
					variants.ref = vcf_temp.ref AND
					variants.alt = vcf_temp.alt
				) vid,
				:source_id sid, rsid, allelefreq, clinical_significance, clinical, precious, locus_specific_db
				FROM vcf_temp
			) vsub
			WHERE NOT EXISTS (SELECT variant_id FROM known WHERE known.variant_id = vid AND source_id = sid)
			''', dict(source_id=self.source_id))
		#self.session.execute("DROP INDEX vcf_temp_idx")
		self.session.query(Vcf).delete()
		self.session.commit()

	def add(self, vcf):
		self.n += 1
		self.session.add(vcf)
		#self.vcf_insert.execute(vcf.__dict__)
		if self.n % self.block_size == 0:
			self.commit()


def import_vcf(session, vcfname, fastaref, limit, block_size):
	"""Import a single VCF into the table of known variants"""
	basepath = os.path.basename(vcfname)
	source = KnownSource(basepath)
	session.add(source)
	session.flush()
	print("importing", vcfname, "...")
	importer = Importer(session, source.id, block_size)
	start_time = time.time()
	skipped = 0

	inserted = set()
	try:
		for l, record in enumerate(islice(VcfReader(filename=vcfname), 0, limit)):
			info = record.INFO

			# XXX
			if 'AF' in info:
				afreq = info['AF'][0]
			elif 'GMAF' in info:
				afreq = info['GMAF']
			elif 'MAF' in info:
				afreq = info["MAF"]
				if isinstance(afreq, str):
					eafreq, aafreq, tafreq = afreq.split(',')
					afreq = str(float(eafreq) / 100) # because EVS uses percentages
			else:
				afreq = None
			
			if 'CLN' in info:
				clinical = True
			elif 'CA' in info:
				clinical = info['CA'] != '.'
			elif 'CS' in info:
				clinical = 'non-pathogenic' in info['CS'].split(',') # TODO what about histocompatibility?
			elif 'dbSNP_ClinVar' in info:
				clinical = True
			else:
				clinical = False

			clinical_significance = info.get('SCS', None) or info.get('CS', None)
			if isinstance(clinical_significance, str):
				clinical_significance = CLINICAL_SIGNIFICANCE[clinical_significance]
			precious = 'PM' in info or 'E_C' in info
			locus_specific_db = 'LSD' in info

			for alt in record.ALT:
				alt = bytes(str(alt), 'ascii')
				if alt == b'<DEL>':
					skipped += 1
					continue
				ref = bytes(record.REF, 'ascii')
				pos = record.POS - 1
				if record.CHROM in fastaref:
					refseq = fastaref.get(record.CHROM)
					if refseq[pos:pos+len(ref)] != ref:
						print(refseq[pos:pos+len(ref)], ref)
						skipped += 1
						continue
					norm_pos, norm_ref, norm_alt = normalized_indel(
						refseq, pos, ref, alt)
				else:
					norm_pos, norm_ref, norm_alt = pos, ref, alt

				is_transition, is_transversion = transition_transversion(ref, alt)

				key = (record.CHROM, norm_pos, norm_ref, norm_alt)
				if not key in inserted:
					# make sure a variant_id for this SNP exists
					vcf = Vcf(record.CHROM, norm_pos, norm_ref, norm_alt,
						record.ID, afreq, clinical_significance, clinical,
						precious, locus_specific_db,
						is_transition, is_transversion)

					importer.add(vcf)
					inserted.add(key)

				if importer.n % 100000 == 0:
					print("{:9,} records ({:.2f}ms/record). skipped: {:,}".format(importer.n, (time.time() - start_time) / importer.n * 1000, skipped))
	except (Exception, BaseException) as e:
		print("Error parsing line {} of vcf-file:".format(l))
		raise e

	importer.commit()
	if importer.n == 0:
		return
	print(" finished.")
	diff = time.time() - start_time
	print('{:9,} records imported in {:.2f} minutes (skipped: {:,}). {:.2f} ms per record'.format(
		importer.n, diff / 60, skipped, diff / importer.n * 1000))


def vcf_source_id(session, vcfname):
	"""Retrieve the source_id from the database for the given vcfname"""
	basepath = os.path.basename(vcfname)
	return session.query(KnownSource.id).filter(KnownSource.name == basepath).first()


def delete_vcf(session, source_id):
	"""delete all records belonging to a vcf given by its source_id"""
	# TODO this could be solved with cascading deletes
	session.query(KnownVariant).filter(KnownVariant.source_id == source_id).delete()
	session.query(KnownSource).filter(KnownSource.id == source_id).delete()


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("--clear-existing", action='store_true', default=False,
		help="Overwrite existing VCF entries instead of skipping them")
	parser.add_argument("--limit", type=int, default=None, metavar="N",
		help="Import only the first N variants of each given VCF file (default: no limit)")
	parser.add_argument("--block", type=int, default=10000,
		help="Import into the database in blocks of this many records.")
	parser.add_argument("--url", "-u", default=None,
		help="Database configuration URL. If this is missing, the configuration is read from the exomate configuration file")
	parser.add_argument("--verbose", "-v", action='store_true', default=False,
		help="show SQL statements (enables SQLalchemy echoing)")
	parser.add_argument("fasta", metavar="FASTAREF",
		help="Reference sequence in FASTA format. Must have been indexed by samtools faidx")
	parser.add_argument("vcf", metavar="VCF", nargs='+',
		help="file with variants in VCF format (may be gzipped)")
	args = parser.parse_args()

	Session = database.get_session(url=args.url)
	session = Session()
	print("connected to:", session.bind.engine)
	if args.verbose:
		session.bind.engine.echo = True
	Vcf.__table__.create(bind=session.bind)

	fastaref = IndexedFasta(args.fasta)
	for vcfname in args.vcf:
		source_id = vcf_source_id(session, vcfname)
		if source_id is not None:
			if args.clear_existing:
				print(vcfname, "is already in the database, removing records ...")
				delete_vcf(session, source_id)
				session.commit()
			else:
				print(vcfname, "is already in the database, skipping.")
				continue
		import_vcf(session, vcfname, fastaref, args.limit, args.block)
		session.commit()
	session.close()


if __name__ == '__main__':
	main()
