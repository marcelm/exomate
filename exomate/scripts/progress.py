#!/usr/bin/env python3
"""
fuctions for importstat database management
"""
from exomate.database import get_session
from exomate.models import *
from sqlalchemy.sql.functions import now

# TODO rename sample to accession in order to be more general

def update(sample, job):
	# TODO deactivate this for now.
	return
	session = get_session()
	exists = session.query(ImportStat).filter(ImportStat.sample_accession == sample).count()
	if (exists):
		stat = session.query(ImportStat).filter(ImportStat.sample_accession == sample).first()
		stat.current_job = job
		stat.created = now()
		session.commit()
	else:
		stat = ImportStat(sample_accession=sample,current_job=job)
		session.add(stat)
		session.commit()
	session.close()

def finish(sample):
	session = get_session()
	exists = session.query(ImportStat).filter(ImportStat.sample_accession == sample).count()
	if (exists):
		session.query(ImportStat).filter(ImportStat.sample_accession == sample).delete()
		session.commit()
	session.close()
