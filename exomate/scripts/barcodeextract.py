#!/usr/bin/env python3
"""
Try to extract the barcode information of each unit, by scanning the filename
and the header of all .gz files in a given directory and subdirectories
"""
import gzip
import re
import os
import fnmatch
from subprocess import Popen, PIPE
from os.path import basename
from glob import glob
from sqlalchemy.engine.url import make_url
import psycopg2
import exomate.database
from exomate.commandline import HelpfulArgumentParser


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("--url", "-u", default=None,
		help="Database configuration URL. If this is missing, the configuration is read from the exomate configuration file")
	parser.add_argument("gzdir", metavar="GZDIRECTORY", help="The directory to the .gz files.")
		
	args = parser.parse_args()

	if args.url is None:
		url = exomate.database.database_url()
	else:
		url = make_url(args.url)
	if url.drivername != 'postgresql':
		print("Sorry, the configured database driver is {}, but only postgresql is supported by this script".format(url.drivername))
		sys.exit(1)
		
	connect_params = {
		"user": url.username,
		"password": url.password,
		"host": url.host,
		"database": url.database
	}
	connect_params = { k:v for k,v in list(connect_params.items()) if v != '' and v is not None }
	print("connecting to database '{database}', user '{user}' on host '{host}'".format(
		database=url.database, user=url.username, host=url.host))
	connection = psycopg2.connect(**connect_params)
	cursor = connection.cursor()
	
	cursor.execute("SELECT prefix FROM units")
	
	# fetch all.gz files in directory & subdirectories
	possible_files = []
	for root, dirnames, filenames in os.walk(args.gzdir):
		for filename in fnmatch.filter(filenames, '*.gz'):
			possible_files.append(os.path.join(root, filename))
	
	print(possible_files)
	
	acgt = re.compile("[ACGT]{6}")
	
	for unit in cursor.fetchall():
		print(unit[0])
		for p in possible_files:
			if basename(p).startswith(unit[0]):
				print("file found:", basename(p))
				# try to extract barcode from name
				m = acgt.search(basename(p))
				if m is not None:
					# barcode in filename
					barcode =  m.group(0)
					print("extracted from name", barcode)
					write_barcode(cursor, unit[0], barcode)
				else:
					# try to open file and extract from header
					f = gzip.open(p)
					barcode = f.readline().splitlines()[0].split(":")[-1]
					if (acgt.match(barcode)): #check if pattern is correct, size 6 and contains only ACGT
						print("extracted from header", barcode)
						write_barcode(cursor, unit[0], barcode)
						break
					f.close()
	
	connection.commit()
	connection.close()


def write_barcode(cursor, unit, barcode):
	cursor.execute("UPDATE units SET barcode='%s' WHERE prefix='%s'" % (barcode,unit))


if __name__ == '__main__':
	main()
