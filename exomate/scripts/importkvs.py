"""Import KVS files into the database.

KVS files have the extension .kvs. They consist of lines that look like
this:

unit U001234 cov1 = 0.17

This provides information about the unit U001234 and it says that the value cov1
should be set to 0.17. In the database, the column 'cov1' of the table
unit_stats will be set to the value 0.17 for the unit that has accession
"U001234". If no row exists for such a unit, it will first be created.
The suffix _stats is hardcoded.
"""

import sys

from sqlalchemy import Boolean

from exomate.database import get_session
from exomate.models import UnitStats, Unit
from exomate.commandline import HelpfulArgumentParser

__author__ = "Marcel Martin"

db_session = get_session()


def parse_value(v):
	"""
	"""
	try:
		v = int(v)
	except ValueError:
		try:
			v = float(v)
		except ValueError:
			pass
	return v


def import_kvs(filename):
	"""
	"""
	with open(filename) as f:
		for line in f:
			fields = line.split()
			if len(fields) != 5:
				continue
			ttype, name, column, _, value = fields
			assert ttype == 'unit', "only 'unit' is supported"

			# TODO this cannot be the right way to do this
			if type(UnitStats.__table__.columns.__getattr__(column).type) is Boolean:
				if value.lower() in ['yes', 'true', '1']:
					value = True
				elif value.lower() in ['no', 'false', '0']:
					value = False
				else:
					raise ValueError("value must be boolean (true, false, yes or no expected)")
			else:
				value = parse_value(value)

			unit = db_session.query(Unit).filter(Unit.prefix == name).first()
			if unit is None:
				print("unit", name, "not found, skipping")
				continue

			if unit.stats is None:
				unit.stats = UnitStats(**{column: value, 'unit': unit })
			else:
				unit.stats.__setattr__(column, value)
	db_session.commit()


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("kvs", nargs="+")
	args = parser.parse_args()

	n = 0
	for name in args.kvs:
		import_kvs(name)
		n += 1
	db_session.remove()
	print(n, "kvs files imported")


if __name__ == '__main__':
	main()
