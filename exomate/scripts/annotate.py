#!/usr/bin/env python3
# kate: syntax python; remove-trailing-space-save off; remove-trailing-space off;
"""
Complete annotations table in reference to variants
and features tables.

  Determine the transcript id, the old codon, the new (mutated) codon and their acids, of given variants and feature tables

  Warning, codons, where feature of length 1 building the center nucleotid, are not calculated correctly
	 |A   C   G|
  |----| |-| |------------|

"""
import sys
import psycopg2
from exomate.commandline import HelpfulArgumentParser
from sqt.io.fasta import IndexedFasta
from sqt.dna import GENETIC_CODE
from psycopg2.extras import NamedTupleCursor
from exomate.variants import VARIANT_TYPES, VARIANT_REGIONS, enum
from exomate.database import database_url

__author__ = "Christopher Schröder, Manuel Allhoff, Marcel Martin"

#TODO utr -> 5'utr, 3'utr, downstream, upstream

REGION = enum(STOP=5, CDS_START=4, EXON=3, EXON_SPLICE=2, STOP_SPLICE=1, CDS_START_SPLICE=0)

# the special "amino acid" characters that are used to describe start/stop codons
START = '+'
STOP = '*'

features = {}

def fetch_chromosomes(connection):
	print("Fetching list of chromosome names ...")
	cursor = connection.cursor(cursor_factory=NamedTupleCursor)
	cursor.execute("SELECT DISTINCT chrom FROM transcripts")
	return [row.chrom for row in cursor]


def get_codon_start_pos(pos, frame, start):
	"""Compute position of variant within codon.
	Return 0, 1, or 2"""
	return pos - ((pos - start - frame) % 3)


def get_reverse_frame(start, stop, frame):
	"""Return the frame_shift on the the features other end."""
	return (stop - start - frame) % 3


def overlap_left(a, frame, is_forward):
	"""Check if variant overlaps on right side."""
	if is_forward:
		left_frame = frame
	else:
		left_frame = get_reverse_frame(a.start, a.stop, frame)
	return a.pos < a.start + left_frame


def overlap_right(a, frame, is_forward):
	"""Check if variant v overlaps left side."""
	if is_forward:
		right_frame = get_reverse_frame(a.start, a.stop, frame)
	else:
		right_frame = frame
	return a.pos >= a.stop - right_frame


def reverse_complement(sequence):
	"""Calculate reverse complement. Characters not in ACGTN are removed."""
	complement = {'A':'T','C':'G','G':'C','T':'A','N':'N'}
	return "".join([complement.get(nt, '') for nt in sequence[::-1]])


def write_annotation_run(connection):
	"""Write the new max id to the annotation_run-table"""
	print("write annotation run")
	cursor = connection.cursor()
	cursor.execute("SELECT max(max_variant_id) FROM annotation_run")
	max_prev_id = cursor.fetchone()[0]
	cursor.execute("SELECT max(id) FROM variants")
	max_id = cursor.fetchone()[0]
	if max_prev_id != max_id:
		cursor.execute("INSERT INTO annotation_run (max_variant_id) VALUES (%s)", (max_id,))
	return max_id


def get_min_variant_id(connection):
	min_variant_id_cursor = connection.cursor(cursor_factory=NamedTupleCursor)
	min_variant_id_cursor.execute("SELECT max(max_variant_id) as max_var_id FROM annotation_run")
	max_var_id = min_variant_id_cursor.fetchone().max_var_id
	if max_var_id is None:
		max_var_id = -1
	return max_var_id



def annotate_all(connection, chromosomes, fastafile):
	"""Annotate all chromosomes."""

	print("Opening reference FASTA file ...")
	indexedfasta = IndexedFasta(fastafile)

	min_variant_id = get_min_variant_id(connection)
	print("Largest previously annotated variant id is ", min_variant_id)
	for chrom in sorted(chromosomes):
		variant_id = annotate_chrom(connection, indexedfasta, chrom, min_variant_id)
	print("finished")



def annotate_chrom(connection, indexedfasta, chrom, min_variant_id):
	"""Fetch features on single chromosome."""
	print("")
	print("Fetching features on chromosome", chrom, "...")
	cursor = connection.cursor(cursor_factory=NamedTupleCursor)

	features = {}

	# save all features
	cursor.execute("SELECT f.*, t.chrom, t.strand, t.source FROM features f JOIN transcripts t ON t.id = f.transcript_id WHERE t.chrom = %s AND t.source='protein_coding'", (chrom, ))
	for f in cursor:
		features[(f.transcript_id, f.type, f.exon_number)] = f;

	if not len(features):
		print("No Features")
		return


	# fetch all hits of each variant
	print("Fetching Hits ...")
	cursor.execute("""
			SELECT v.id as vid, f.id as fid, v.chrom, f.type, f.transcript_id, v.pos, f.start, f.stop, v.alt, v.ref, f.exon_number, f.frame, t.strand
			FROM variants v JOIN features f ON v.pos BETWEEN f.start-10 AND f.stop+9 JOIN transcripts t ON t.id = f.transcript_id WHERE v.chrom = %s AND t.chrom = %s AND t.source='protein_coding' AND v.id > %s
		""",  (chrom, chrom, min_variant_id))

	lastout = 0
	count = 0

	# filter for best hit type
	# determine in which region a variants hits
	# a variant can hit an multiple regions of the same transcript, for example exon and cds or cds - 2 and stop
	# each region gets a score, and the highest score of all hits of an variant on the same transcript shows the most important one
	# the scores are shown in get_region(a)
	bestfilter = {}

	print("Filtering Hits...")
	print(0, end='')
	for a in cursor:
		# some statistic stuff
		count += 1
		if (count > lastout + 9999):
			print('%s\r' % ' '*30, end='')
			print("Variants filtered:", count, end='')
			lastout = count

		region = get_region(a)
		if region > bestfilter.get((a.vid, a.transcript_id), (None, -1))[1]:
			bestfilter[(a.vid, a.transcript_id)] = (a, region)

	print("")
	print (len(bestfilter), "annotations remaining after filtering")

	print("Writing Hits")

	lastout = 0
	count = 0

	for a, region in bestfilter.values():
		count += 1
		if (count > lastout + 9999):
			print('%s\r' % ' '*30, end='')
			print("Variants written:", count, end='')
			lastout = count

		# calculate the distance to the feature
		# positive if between start and end
		dist = min(a.pos - a.start, a.stop - 1 - a.pos)

		if region == REGION.CDS_START:
			save_coding(a, features, dist, indexedfasta, cursor)
		elif region == REGION.STOP:
			save_coding(a, features, dist, indexedfasta, cursor)
		elif region == REGION.EXON:
			save_utr(a, dist, cursor)
		elif region == REGION.EXON_SPLICE:
			save_intron(a, dist, cursor)
		elif region == REGION.STOP_SPLICE:
			save_intron(a, dist, cursor)
		elif region == REGION.CDS_START_SPLICE:
			save_intron(a, dist, cursor)
		else:
			print("Error - this should not happen")

	print("")
	print(count, "annotations written")


def get_region(a):
	"""
	Return the score of the hit.
	stop = 5
	cds/start = 4
	exon = 3
	exon_splice = 2
	stop_splice = 1
	cds_splice / start_splice = 0
	"""
	dist = min(a.pos - a.start, a.stop - 1 - a.pos)
	if a.type == "exon":
		if dist < 0:
			return REGION.EXON_SPLICE
		return REGION.EXON
	if a.type == "CDS" or a.type == "start_codon":
		if dist < 0:
			return REGION.CDS_START_SPLICE
		return REGION.CDS_START
	elif a.type == "stop_codon":
		if dist < 0:
			return REGION.STOP_SPLICE
		return REGION.STOP
	else:
		print("error", a.type)


def save_utr(a, dist, cursor):
	"""Save UTR Hit to Database."""
	write_db(a.vid, a.fid, '', '', '', '', VARIANT_TYPES.NOCODING, VARIANT_REGIONS.UTR, dist, cursor)


def save_intron(a, dist, cursor):
	"""Save Intron Hit to Database."""
	write_db(a.vid, a.fid, '', '', '', '', VARIANT_TYPES.NOCODING, VARIANT_REGIONS.INTRON, dist, cursor)


def save_coding(a, features, dist, indexedfasta, cursor):
	"""Save Hit on Coding Region to Database."""
	if len(a.alt) == len(a.ref) == 1:
		# no insertion, no deletion, possible types:
		# synonymous
		# nonsense
		# missense

		# set direction
		is_forward = (a.strand == '+')
		# frame: none -> 0
		frame = 0 if (a.frame is None) else a.frame
		# detecting frame on the left site
		left_frame = frame if is_forward else get_reverse_frame(a.start, a.stop, frame)

		if overlap_left(a, frame, is_forward):
			# variant overlap left side
			if is_forward:
				feat1 = features.get((a.transcript_id, a.type, a.exon_number - 1), None)
				feat2 = features.get((a.transcript_id, a.type, a.exon_number), None)
			else:
				feat1 = features.get((a.transcript_id, a.type, a.exon_number + 1), None)
				feat2 = features.get((a.transcript_id, a.type, a.exon_number), None)
			if not (feat1 == None or feat2 == None):
				sequence, new_sequence = get_overlap_sequence(a, feat1, feat2, indexedfasta)
			else:
				#missing_features_count +=1
				return
		elif overlap_right(a, frame, is_forward):
			# variant overlap right side
			if is_forward:
				feat1 = features.get((a.transcript_id, a.type, a.exon_number), None)
				feat2 = features.get((a.transcript_id, a.type, a.exon_number + 1), None)
			else:
				feat1 = features.get((a.transcript_id, a.type, a.exon_number), None)
				feat2 = features.get((a.transcript_id, a.type, a.exon_number - 1), None)
			if not (feat1 == None or feat2 == None):
				sequence, new_sequence = get_overlap_sequence(a, feat1, feat2, indexedfasta)
			else:
				#missing_features_count += 1
				return
		else:
			# no overlap
			codon_start = get_codon_start_pos(a.pos, left_frame, a.start)
			sequence = indexedfasta.get(a.chrom)[codon_start:(codon_start+3)].decode()
			new_sequence = replace_position(sequence, a.pos - codon_start, a.alt)

		#reverse complement?
		if not is_forward:
			sequence = reverse_complement(sequence)
			new_sequence = reverse_complement(new_sequence)

		#detecting the aminoacids
		acid = START if (a.type == 'start_codon') else GENETIC_CODE.get(sequence, STOP)
		new_acid = GENETIC_CODE.get(new_sequence, STOP)

		# determine mutation type (synonymous, missense, ...)
		mutation_type = get_mutation_type_by_acid(acid, new_acid)
		write_db(a.vid, a.fid, sequence, acid, new_sequence, new_acid, mutation_type, VARIANT_REGIONS.CODING, dist, cursor)
		#inserted_count_normal = inserted_count_normal + 1
	else:
		# len(ref codon) != 1, len(alt condon) != 1
		# must be insertion or deletion or other
		mutation_type = get_mutation_type(a.ref, a.alt)
		write_db(a.vid, a.fid, '', '', '', '', mutation_type, VARIANT_REGIONS.CODING, dist, cursor)


def get_overlap_sequence(a, f1, f2, indexedfasta):
	"""Determining the codon sequence of an overlap variant."""
	#determine the left frame shift
	if f2.strand == '+':
		left_frame = f2.frame
	else:
		left_frame = get_reverse_frame(f2.start, f2.stop, f2.frame)

	#calculating the sequence
	sequence = indexedfasta.get(a.chrom)[(f1.stop - 3 + left_frame):f1.stop].decode() + indexedfasta.get(a.chrom)[f2.start:(f2.start + left_frame)].decode()
	if a.pos >= f2.start:
		new_sequence = replace_position(sequence, 3 - left_frame - f2.start + a.pos, a.alt)
	else:
		new_sequence = replace_position(sequence, 3 - left_frame - f1.stop + a.pos, a.alt)

	#reverse complement?
	if f1.strand == '-':
		sequence = reverse_complement(sequence)
		new_sequence = reverse_complement(new_sequence)

	return (sequence, new_sequence)


def replace_position(seq, pos, char):
	"""Return a copy of the string seq in which
	the character at position pos has been replaced by char."""
	return seq[:pos] + char + seq[pos+1:]


def write_db(variant_id, feature_id, old_codon, old_acid, new_codon, new_acid, type, region, dist, cursor):
	cursor.execute("INSERT INTO annotations (variant_id, feature_id, ref_codon, alt_codon, ref_acid, alt_acid, type, region, splice_dist) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", (variant_id, feature_id, old_codon, new_codon, old_acid, new_acid, type, region, dist))


"""Return the mutation type by acid"""
def get_mutation_type_by_acid(refAcid, altAcid):
	# determine mutation type
	if refAcid == altAcid:
	# synonymous
		return VARIANT_TYPES.SYNONYMOUS;
	elif altAcid == STOP:
	# nonsense
		return VARIANT_TYPES.NONSENSE;
	elif refAcid == STOP:
	# readthrough
		return VARIANT_TYPES.READTHROUGH;
	else:
	# missense
		return VARIANT_TYPES.MISSENSE;


def get_mutation_type(refSeq, altSeq):
	'''Return the mutation type by seqence'''
	if len(refSeq) > len(altSeq) and refSeq.startswith(altSeq):
		# deletion
		return VARIANT_TYPES.DELETION
	elif len(refSeq) < len(altSeq) and altSeq.startswith(refSeq):
		# insertion
		return VARIANT_TYPES.INSERTION
	else:
		# no insertion and no deletion, e.g. ACGT -> ACAAC
		return VARIANT_TYPES.OTHER


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("--delete", action='store_true', default=False,
		help="Delete all annotations and recompute")
	parser.add_argument("--url", "-u", default=None,
		help="Database configuration URL. If this is missing, the configuration is read from the exomate configuration file")
	parser.add_argument("fasta", metavar="FASTA",
		help="the reference in FASTA format, indexed by samtools faidx")
	args = parser.parse_args()
	if args.url is None:
		url = database_url()
	else:
		url = make_url(args.url)
	if url.drivername != 'postgresql':
		print("Sorry, the configured database driver is {}, but only postgresql is supported by this script".format(url.drivername))
		sys.exit(1)
	# set up connection arguments
	connect_params = {
		"user": url.username,
		"password": url.password,
		"host": url.host,
		"database": url.database
	}

	connect_params = { k:v for k,v in connect_params.items() if v != '' and v is not None }
	print("connecting to database '{database}', user '{user}' on host '{host}'".format(
		database=url.database, user=url.username, host=url.host))
	connection = psycopg2.connect(**connect_params)


	cursor = connection.cursor()
	if args.delete:
		print("Deleting existing annotations ...")
		cursor.execute("DELETE FROM annotations")
		cursor.execute("DELETE FROM annotation_run")

	# force PostgreSQL to collect statistics
	# If this is not done, some queries that join the variants and features table
	# will be not be optimized well and take an extremely long time to execute.
	print("analyzing tables ...")
	cursor.execute("ANALYZE variants")
	cursor.execute("ANALYZE features")
	chromosomes = fetch_chromosomes(connection)

	annotate_all(connection, chromosomes, args.fasta)
	write_annotation_run(connection)
	connection.commit()

	# Vacuuming must be done after a large amount of data has been imported.
	# Since this script is run after such imports, do the vacuuming here.
	# If you do not do this, the query times will suffer! (It seems this is
	# the case even with autovacuuming enabled.)
	print("Vacuuming ...")

	connection.set_isolation_level(0)
	cursor.execute("VACUUM ANALYZE")
	connection.commit()
	connection.close()


if __name__ == '__main__':
	main()
