#!/usr/bin/env python3
"""
Debugging tool for exomate, which spawns an interactive shell in which
an SQLalchemy session object exists that can be used to query the
database.

History is saved across sessions and kept in ~/.exomate_history.
"""


import os
import sys
import atexit
import code
from textwrap import wrap

from sqlalchemy import or_, and_, not_, between, distinct, func, desc, asc
from sqlalchemy.orm import aliased

from exomate.commandline import HelpfulArgumentParser
from exomate.database import get_session
from exomate.models import *

__author__ = "Marcel Martin"


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument('--echo', '-e', action='store_true', default=False,
		help='Echo SQL commands')
	args = parser.parse_args()
	Session = get_session()

	session = Session()
	session.bind.echo = args.echo

	try:
		import readline
	except ImportError:
		print("Module readline not available.")
	else:
		import rlcompleter
            
	histfile = os.path.expanduser('~/.exomate_history')
	try:
		readline.read_history_file(histfile)
	except IOError:
		pass
	atexit.register(readline.write_history_file, histfile)

	env = globals().copy()
	env['session'] = session
	env['s'] = session
	for o in ('atexit', 'code', 'HelpfulArgumentParser'):
		del env[o]
	readline.parse_and_bind("tab: complete")
	readline.set_completer(rlcompleter.Completer(namespace=env).complete)
	banner = 'Exomate interactive shell started.\n\n'
	banner += 'To enable printing of SQL statements, type "s.bind.echo = True"\n\n'
	banner += 'The following names are available:\n'
	banner += '\n'.join(wrap(', '.join(name for name in env if not name.startswith('_'))))
	banner += '\n'
	code.interact(local=env, banner=banner)


if __name__ == '__main__':
	main()
