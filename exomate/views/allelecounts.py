"""
The allelecounts page
"""

from exomate.database import get_session
from flask import render_template, request
from exomate.application import app
from exomate.models import Sample
from exomate.views.common import get_bam_path, get_pileup
from sys import stderr
from collections import Counter
from itertools import chain

session = get_session()


@app.route('/allelecounts', methods=['GET', 'POST'])
def allele_counts():
	chrom, pos = request.args.get('pos').split(":")
	pos = int(pos) - 1  # convert back to 0-based coordinates
	highlight_samples = request.args.get('highlight_samples')
	highlight_samples = set() if highlight_samples is None else set(s.strip() for s in highlight_samples.split(","))
	samples = session.query(Sample).all()

	bam_path = get_bam_path()
	if not bam_path:
		return render_template('error.html', message="Could not retrieve Bam path")
	counts = []
	alleles = set()
	for sample in samples:
		try:
			sample_alleles, sample_forward, sample_backward = get_allele_counts(sample, chrom, pos, bam_path=bam_path)
		except IOError as e:
			print("Skipping sample {} because bam or index is not accessible: {}".format(sample.accession, e), file=stderr)
			continue
		counts.append((sample, sample_forward, sample_backward))
		alleles.update(sample_alleles)
	counts.sort(key=lambda item: item[0].accession)
	alleles = list('ACGTN') + sorted(allele for allele in alleles if allele not in set('ACGTN'))

	return render_template('allele-counts.html', pos="{}:{}".format(chrom, pos+1), alleles=alleles, counts=counts, highlight_samples=highlight_samples)


def get_allele_counts(sample, chrom, pos, bam_path=None):
	allelecounts = [Counter(), Counter()]
	for base, qual, is_reverse, is_del in get_pileup(sample, chrom, pos, bam_path=bam_path):
		if not is_del:
			allelecounts[is_reverse][base] += 1
		else:
			allelecounts[is_reverse]["Deletion"] += 1

	alleles = set(chain(*allelecounts))

	forward, backward = allelecounts
	return alleles, forward, backward

