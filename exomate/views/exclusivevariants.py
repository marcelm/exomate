"""
The exclusive_variants page
"""

from exomate.database import get_session
from flask import render_template
from exomate.application import app

session = get_session()

@app.route('/exclusive_variants')
def exclusive_variants():
	stats = session.execute('''select
								(select count(c1.variant_id) from calls c1
									left outer join calls c2 on c1.variant_id = c2.variant_id and c2.sample_id != samples.id
									where c1.sample_id = samples.id and c2.variant_id is null
								)
								, samples.accession from samples;''')
	return render_template('exclusive_variants.html', stats = stats)
