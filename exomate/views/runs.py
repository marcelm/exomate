"""
The Runs page
"""

from exomate.database import get_session
from flask import render_template, request
from exomate.application import app
from exomate.models import Run

session = get_session()

@app.route('/runs')
def runs():
	optmachine = request.args.get('machine', None)
	optflowcell = request.args.get('flowcell', None)
	optseqrun = request.args.get('seqrun', None)

	runs = Run.query

	if optmachine is not None:
		if optmachine == "unknown":
			optmachine = ""
		runs = runs.filter(Run.machine == optmachine)

	if optflowcell is not None:
		if optflowcell == "unknown":
			optflowcell = None
		runs = runs.filter(Run.flowcell == optflowcell)

	if optseqrun is not None:
		if optseqrun == "unknown":
			optseqrun = ""
		runs = runs.filter(Run.seqrun == optseqrun)

	runs = runs.order_by(Run.date)
	return render_template('runs.html', runs=list(runs))

