"""
The stats page
"""

from exomate.database import get_session
from flask import render_template, request
from exomate.application import app
from exomate.models import UnitStats, Run, Sample, Disease, Unit, Library, SampleGroup
from sqlalchemy import desc
from exomate.views.common import get_incomplete_annotation_warning

session = get_session()

@app.route('/stats')
def stats():
	optrun = request.args.get('run', None)
	optdisease = request.args.get('disease', None)
	optsamplegroup = request.args.get('samplegroup', None)

	stats = session.query(UnitStats).join(Unit).outerjoin(Run)
	#imported = [x for (x,) in session.query(distinct(Call.unit_id)).all()]
	imported = []

	# run filter
	filteredrun = optrun is not None and not (optrun == "")
	if filteredrun:
		stats = stats.filter(UnitStats.unit.has(Unit.run.has(Run.id == optrun)))
	# disease filter
	filtereddisease = optdisease is not None and not (optdisease == "")
	if filtereddisease:
		stats = stats.filter(UnitStats.unit.has(Unit.library.has(Library.sample.has(Sample.disease.has(Disease.id == optdisease)))))
	# samplegroup filter
	filteredsamplegroup = optsamplegroup is not None and not (optsamplegroup == "")
	if filteredsamplegroup:
		stats = stats.filter(UnitStats.unit.has(Unit.library.has(Library.sample.has(Sample.samplegroups.any(SampleGroup.id == optsamplegroup)))))

	stats = stats.order_by(desc(Run.date), Unit.lane).all()
	warning = get_incomplete_annotation_warning()
	return render_template('stats.html',
				stats=stats,
				imported=imported,
				runs=Run.query,
				diseases=Disease.query,
				samplegroups=SampleGroup.query,
				is_single_run=filteredrun,
				warning=warning,
				optrun=optrun,
				optdisease=optdisease,
				optsamplegroup = optsamplegroup,
				filtered = (filtereddisease or filteredsamplegroup or filteredrun))

