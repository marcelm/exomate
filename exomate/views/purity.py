"""
The purity page
"""

from exomate.database import get_session
from flask import render_template, request
from exomate.application import app
from exomate.models import Disease, Sample, Call, Variant
from exomate.views.common import parse_aux_filter_parameters, get_aux_filter, PurityRow, generalizedESD
from sqlalchemy import or_, func
import numpy as np

session = get_session()

@app.route('/purity', methods=['GET', 'POST'])
def purity():
	"""
	Purity estimation as defined by Su, X. et al, 2012. Bioinformatics 28, 2265–2266.
	"""
	healthy = session.query(Disease.id).filter(Disease.short_name == 'healthy').scalar()
	quality = request.form.get('quality', 50, type=int)

	if request.method != "POST":
		#query the mutated samples wich have a matching healty sample from same patient
		samples = session.execute("""SELECT *
		    FROM samples as one
			WHERE one.disease_id != """ + str(healthy) + """
			AND EXISTS(
				SELECT 1
				FROM samples as two
				WHERE one.patient_id = two.patient_id
				AND two.disease_id = """ + str(healthy) + """);
		 """)
		return render_template("purity-query.html", quality=quality, samples=samples)

	#extract samples from post parameters
	affected_sample_ids = request.form.getlist('samples', type=int)
	affected_samples = session.query(Sample).filter(Sample.id.in_(affected_sample_ids)).all()

	#generate quality filter and aux filter from post parameters
	qual_filter = Call.qual >= quality
	aux_filter_query = parse_aux_filter_parameters(request.form)
	aux_filter = get_aux_filter(aux_filter_query)

	#specify the number of chromosome to inspect per sample (only consider autosomes, hence 22)
	chrom_count = 22

	rows = []

	# only consider coverage above 20 to ensure that quotients do not suffer from small differences
	allele_frequencies = session.query(
		func.sum(Call.alt_depth).label("alt"),
		func.sum(Call.ref_depth).label("ref")).\
		filter(Call.is_heterozygous).\
		filter(qual_filter).\
		join(Variant).filter(aux_filter).\
		filter(or_(Variant.is_transition, Variant.is_transversion))

	mutant_allele_fraction = lambda allele_frequencies: allele_frequencies.alt / (allele_frequencies.alt + allele_frequencies.ref)

	#compute purity for each chrom in each sample
	for sample in affected_samples:
		healthy_samples = session.query(Sample).filter(Sample.patient_id == sample.patient_id).filter(Sample.disease_id == healthy)

		for healthy_sample in healthy_samples:

			chrom_purities = []
			for chrom in range(1, chrom_count + 1):
				allele_frequencies_chrom = allele_frequencies.filter(Variant.chrom == str(chrom))

				# fetch healthy calls
				healthy_calls = session.query(Call).join(Variant).filter(Variant.chrom == str(chrom)).filter(Call.sample_id == healthy_sample.id).subquery()

				# calc allele frequencies for somatic mutations (i.e. mutations minus healthy calls)
				allele_frequencies_affected = allele_frequencies_chrom.filter(Call.sample_id == sample.id).outerjoin(healthy_calls, healthy_calls.c.variant_id == Call.variant_id).filter(healthy_calls.c.variant_id == None).one()
				# calc allele frequencies for germline mutations
				allele_frequencies_healthy = allele_frequencies_chrom.filter(Call.sample_id == healthy_sample.id).one()

				if allele_frequencies_affected.alt is None or allele_frequencies_healthy.alt is None:
					# no calls on chromosome, hence exclude it
					continue
				# calculate the fractions as defined in the PurityEst paper
				mutant_allele_fraction_affected = mutant_allele_fraction(allele_frequencies_affected)
				mutant_allele_fraction_healthy = mutant_allele_fraction(allele_frequencies_healthy)

				# the first fraction is 0.5 if affected sample is pure. Sequencing can introduce bias here, hence the 0.5 is normalized with the fraction for the healthy sample.
				# purity is the quotient of the two fractions
				chrom_purity = mutant_allele_fraction_affected / mutant_allele_fraction_healthy
				print("estimated chromosome purity", chrom_purity, file.stderr)

				# the purity estimate can exceed one (reason can be: two few/too many low quality SNPs in healthy sample). We exclude such estimates.
				if chrom_purity > 1:
					continue
				chrom_purities.append(chrom_purity)

			# calculate the generalized ESD as suggested in the paper
			print("calculating generalized ESD", file.stderr)
			outlier_count, indices = generalizedESD(chrom_purities, len(chrom_purities) // 4)
			print("calculated generalized ESD", file.stderr)
			# add omitted samples to the outlier count
			outlier_count += chrom_count - len(chrom_purities)
			# remove outliers
			indices = set(indices)
			non_outlier_purities = [purity for i, purity in enumerate(chrom_purities) if i not in indices]
			if non_outlier_purities:
				purity = np.mean(non_outlier_purities)
				std = np.std(non_outlier_purities)
			else:
				purity = None
				std = None

			rows.append(PurityRow(sample, healthy_sample, purity, std, outlier_count))

	return render_template('purity-result.html', rows=rows)

