"""
The importstatus page
"""

from flask import render_template
from exomate.application import app
from exomate.models import ImportStat
from sqlalchemy import desc


@app.route('/importstatus')
def importstatus():
	stats = ImportStat.query.order_by(desc(ImportStat.current_job).nullslast()).all()
	return render_template('import-status.html', stats=list(stats))
