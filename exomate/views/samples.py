"""
The sample page
"""

from exomate.database import get_session
from flask import render_template, request
from exomate.application import app
from exomate.models import Sample, KnownSource, KnownVariant, Call, Variant
from sqlalchemy import desc, func, not_

session = get_session()


@app.route('/samples')
def samplelist():
	optsampleaccession = request.args.get('sampleaccession', None)
	optsampletissue = request.args.get('sampletissue', None)
	optpatient = request.args.get('person', None)
	#optpatientaccession = request.args.get('patientaccession', None)

	samples = Sample.query

	if optsampleaccession is not None:
		samples = samples.filter(Sample.accession == optsampleaccession)

	if optsampletissue is not None:
		samples = samples.filter(Sample.tissue == optsampletissue)

	if optpatient is not None:
		samples = samples.filter(Sample.patient.has(Patient.accession == optpatient))

	return render_template('samples.html', samples=list(samples), is_single=(optpatient != None))


@app.route('/sample/<accession>')
def sample(accession):
	quality = request.args.get('quality', 200, type=int)
	# try to get the id of the "most recent" known source
	source = session.query(KnownSource).order_by(desc(KnownSource.name)).first()
	sample = session.query(Sample).filter(Sample.accession == accession).one()
	calls = session.query(Call).filter(Call.sample_id == sample.id).\
		filter(Call.qual >= quality)
	total_count = calls.count()
	# needed for dbSNP re-discovery rate
	dbsnp_count = calls.join(Variant).join(KnownVariant).join(KnownSource).\
		filter(KnownVariant.source == source).count()

	# heterozygous/homozygous call rate
	hom_het = hom_het_ratios(accession, quality)

	return render_template('sample.html',
		sample=sample,
		total_count=total_count,
		dbsnp_count=dbsnp_count,
		quality=quality,
		source=source,
		hom_het=hom_het)


def hom_het_ratios(accession, quality):
	"""
	For a given sample, find the number of homozygous and heterozygous calls
	per chromosome. Return a list of (chrom, count_hom, count_het) tuples.

	accession -- sample accession
	"""
	het = session.query(Variant.chrom, func.count().label("count_het")).\
		select_from(Call).join(Variant).join(Sample).\
		filter(Call.is_heterozygous == True).\
		filter(Call.qual >= quality).\
		filter(Sample.accession == accession).\
		group_by(Variant.chrom).subquery()
	hom = session.query(Variant.chrom, func.count().label("count_hom")).\
		select_from(Call).join(Variant).join(Sample).\
		filter(not_(Call.is_heterozygous)).\
		filter(Call.qual >= quality).\
		filter(Sample.accession == accession).\
		group_by(Variant.chrom).subquery()
	hom_het = session.query(hom.c.chrom, "count_hom", "count_het").\
		select_from(hom).join(het, hom.c.chrom == het.c.chrom).order_by(hom.c.chrom).all()
	return [ x for x in hom_het if not x[0].startswith('GL') ]

