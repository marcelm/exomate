"""
The index page
"""

from exomate.database import get_session
from flask import render_template, request
from exomate.application import app
from exomate.models import Variant, Call, KnownVariant
from exomate.views.common import get_incomplete_annotation_warning
from sqlalchemy import between

session = get_session()


#@shorten("variants")
@app.route('/variants', methods=['GET', 'POST'])
def variants():
	ppos = request.args.get('pos')
	if request.method == 'GET' and not ppos:
		warning = get_incomplete_annotation_warning()
		return render_template('variants-query.html', warning=warning)

	context = request.form.get('context', 10, type=int)
	show_id = request.form.get('show_variant_id', None) == 'yes'

	regions = []
	highlight_pos = set()
	not_found = [] # list of strings
	coordinates = request.form.get('coordinates', '').split()
	if ppos:
		coordinates.append(ppos)
	for s in coordinates:
		try:
			if s.find("-") != -1:
				# a region was given
				region = parse_region(s)
				regions.append(region)
			else:
				# a single coordinate was given
				chrom, pos = parse_coordinate(s)
				region = (chrom, pos - context, pos + context)
				regions.append(region)
				highlight_pos.add((chrom, pos))
		except ValueError:
			not_found.append(s)

	# query the variant ids
	variants = []
	for (chrom, start_pos, stop_pos) in regions:
		# "BETWEEN" isn't well defined in SQL. We use it such that potentially
		# one more position is included than requested.
		results = session.query(Variant).filter(Variant.chrom == chrom).\
			filter(between(Variant.pos, start_pos, stop_pos)).all()
		if not results:
			not_found.append("{}:{}-{}".format(chrom, start_pos + 1, stop_pos))
		variants.extend(result.id for result in results)

	if variants:
		calls = session.query(Call).filter(Call.variant_id.in_(variants)).all()
		known = session.query(KnownVariant).filter(KnownVariant.variant_id.in_(variants)).all()
	else:
		calls = known = []
	calls = sorted(calls, key=lambda call: (call.variant.chrom, call.variant.pos))
	warning = get_incomplete_annotation_warning()
	return render_template('variants-result.html', calls=calls, known=known,
		not_found=not_found, show_id=show_id, warning=warning, highlight_pos=highlight_pos)


def parse_coordinate(s):
	"""
	Parse string into a (chromosome, position) tuple

	>>> parse_coordinate("17:1234")
	("17", 1233)
	>> parse_coordinate("chr5:2,345,599")
	("5", 2345598)
	"""
	chrom, pos = s.split(':', 1)
	chrom = chrom.upper()
	if chrom.startswith('CHR'):
		chrom = chrom[3:]
	pos = pos.replace(",", "")
	pos = int(pos) - 1
	return chrom, pos


def parse_region(s):
	"""
	Parse a region string into a (chromosome, start_pos, stop_pos) tuple.

	start_pos, stop_pos are a half-open pythonic interval.

	>>> parse_region("17:1234-5555")
	("17", 1233, 5555)
	>>> parse_region("chr14:5,123,651-22,110,543")
	("14", 5123650, 22110543)
	"""
	chrom, pos = s.split(':', 1)
	pos = pos.replace(",", "")
	chrom = chrom.upper()
	if chrom.startswith('CHR'):
		chrom = chrom[3:]
	start_pos, stop_pos = pos.split('-', 1)
	return chrom, int(start_pos) - 1, int(stop_pos)

