"""
The knownstats page
"""

from exomate.database import get_session
from flask import render_template
from exomate.application import app
from exomate.models import KnownVariant, KnownSource
from sqlalchemy import func

session = get_session()

@app.route('/knownstats')
def knownstats():
	stats = session.query(
		func.count(KnownVariant.variant_id).label('count'),
		KnownVariant.source_id).group_by(KnownVariant.source_id).from_self().\
			join(KnownSource).\
			add_column(KnownSource.name)
	return render_template('knownstats.html', stats=stats)


