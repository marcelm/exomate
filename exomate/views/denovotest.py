"""
The denovotest page
"""

from exomate.database import get_session
from flask import render_template, request
from exomate.application import app
from exomate.models import Sample
from exomate.views.common import get_bam_path, get_pileup
from exomate.helper import get_family_members
from math import exp
from exomate.denovotest import prob_denovo_mutation

session = get_session()

@app.route('/denovotest', methods=['GET', 'POST'])
def denovo_test():
	chrom, pos = request.args.get('pos').split(":")
	pos = int(pos) - 1  # convert back to 0-based coordinates
	ref = request.args.get('ref')
	alt = request.args.get('alt')
	samples = request.args.get('samples')
	samples = set() if samples is None else set(s.strip() for s in samples.split(","))
	samples = session.query(Sample).filter(Sample.accession.in_(samples)).all()

	bam_path = get_bam_path()
	if not bam_path:
		return render_template('error.html', message="Could not retrieve Bam path")

	def filter_pileup(sample):
		return [(base, qual) for base, qual, is_reverse, is_del in get_pileup(sample, chrom, pos, bam_path=bam_path) if not is_del]

	def get_denovo_probs(sample):
		sample_pileup = filter_pileup(sample)
		for member in get_family_members(sample.patient):
			for member_sample in member.samples:
				if member_sample != sample:
					member_pileup = filter_pileup(member_sample)
					yield member_sample, min(1.0, exp(prob_denovo_mutation(member_pileup, sample_pileup, ref, alt)))

	return render_template('denovotest.html', pos="{}:{}".format(chrom, pos+1), ref=ref, alt=alt, samples=samples, denovo_probs=get_denovo_probs)

