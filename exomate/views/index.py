"""
The index page
"""

from exomate.database import get_session
from flask import render_template
from exomate.application import app
from exomate.models import Log

session = get_session()

@app.route('/')
def index():
	logs = session.query(Log).order_by(Log.created.desc()).limit(10).all()
	return render_template('index.html', logs=logs)

