"""
The diskusage page
"""

from exomate.database import get_session
from flask import render_template
from exomate.application import app

session = get_session()

@app.route('/info/diskusage')
def disk_usage():
	rows = session.execute("""SELECT nspname || '.' || relname AS "relation",
		pg_size_pretty(pg_relation_size(C.oid)) AS "size"
		FROM pg_class C
		LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
		WHERE nspname NOT IN ('pg_catalog', 'information_schema')
		ORDER BY pg_relation_size(C.oid) DESC
		LIMIT 20;""")
	return render_template('diskusage.html', rows=rows)

