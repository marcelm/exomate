"""
The gendercheck page
"""

from exomate.database import get_session
from flask import render_template
from exomate.application import app
from exomate.models import MappingCount
from sqlalchemy.orm import aliased

session = get_session()


@app.route('/gendercheck')
def gender_check():
	MC_Y = aliased(MappingCount)
	counts = session.query(MappingCount, MC_Y).\
		filter(MappingCount.chrom == 'X').\
		join(MC_Y, MC_Y.unit_id == MappingCount.unit_id).\
		filter(MC_Y.chrom == 'Y').order_by(1.0 * MC_Y.mapped / MappingCount.mapped).all()
	return render_template('gender-check.html', counts=counts)
