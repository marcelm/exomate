"""
The sampleoverlap page
"""

from exomate.database import get_session
from flask import render_template, request
from exomate.application import app
from exomate.models import Sample, Call, Patient
from sqlalchemy import func, or_, distinct
from functools import lru_cache
from itertools import combinations
from collections import defaultdict

session = get_session()

#@shorten('sampleoverlap')
@app.route('/sampleoverlap', methods=['GET', 'POST'])
def sample_overlap():
	quality = request.form.get('quality', 50, type=int)
	samples = session.query(Sample).all()
	if request.method != "POST":
		return render_template("sample-overlap-query.html", quality=quality, samples = samples)


	intersection = '''
		select count(*) from calls c
			join (select * from calls c where c.sample_id = {sampleid1} and c.qual > {quality}) as c2
			on c.variant_id = c2.variant_id and c.sample_id = {sampleid2} where c.qual > {quality};
		'''


	#extract samples from post parameters
	affected_sample_ids = request.form.getlist('samples', type=int)
	affected_samples = session.query(Sample).filter(Sample.id.in_(affected_sample_ids)).all()

	qual_filter = Call.qual >= quality

	if (1 < len(affected_samples) <= 3):
		samples = affected_samples

		callcounts = dict(session.query(Sample.accession, func.count(Call.sample_id)).outerjoin(Call).filter(or_(Call.qual >= quality, Call.qual == None)).group_by(Sample.accession).filter(Sample.id.in_([s.id for s in samples])).all())

		intersectionALLpre = session.query(Call.variant_id).filter(Call.sample_id.in_([s.id for s in samples])).filter(Call.qual>quality).group_by(Call.variant_id).having(func.count(Call.sample_id) == len(samples)).subquery()

		intersectionAll = session.query(func.count(intersectionALLpre.c.variant_id)).one()[0]

		@lru_cache(maxsize=32)
		def common(x,y,z=None):
			if not z:
				return session.execute(intersection.format(sampleid1=x.id, sampleid2=y.id, quality=quality)).fetchone()[0]
			else:
				return intersectionAll

		@lru_cache(maxsize=32)
		def excl(x):
			other = [s for s in samples if not s == x]
			ret = callcounts[x.accession]
			for o in other:
				ret -= common(x, o)
			if len(samples) == 3:
				ret += common(*(s for s in samples))
			return ret

		union = session.query(func.count(distinct(Call.variant_id))).filter(Call.sample_id.in_([s.id for s in samples])).filter(Call.qual>quality).one()[0]

		if not union:
			union = 1  # ensure that ratios become 0

		return render_template('sample-overlap-result-3way.html',
				quality=quality, samples=samples,
				excl=excl,
				union = union,
				common = common,
				combinations=combinations,
				callcounts = callcounts)

	elif not (affected_samples):
		intersection = '''
		select
			(select count(*) from calls c join
				(select * from calls c where c.sample_id = product.id and c.qual > {quality}) as c2
				on c.variant_id = c2.variant_id and c.sample_id = product.id2 where c.qual > {quality})
			,product.accession, product.accession2
		from
		(select a.id, b.id2, a.accession, b.accession2 from
		(
		(select s.id, s.accession from patients p join samples s on s.patient_id = p.id where p.accession = '{patient}') as a
		join
		(select s.id as id2, s.accession as accession2 from patients p join samples s on s.patient_id = p.id where p.accession = '{patient}') as b
		on a.id > b.id2
		)
		)
		as product
		;
		'''
		patients = session.query(Patient).join(Sample).group_by(Patient).having(func.count(Sample.id) > 1).all()
		stats = defaultdict(list)
		def stats(patient):
			callcounts = dict(session.query(Sample.accession, func.count(Call.sample_id)).outerjoin(Call).filter(or_(Call.qual >= quality, Call.qual == None)).join(Patient).group_by(Sample.accession).filter(Patient.id == patient.id).all())
			for count, sample1, sample2 in session.execute(
				intersection.format(patient=patient.accession, quality=quality)):
				excl1 = callcounts[sample1] - count
				excl2 = callcounts[sample2] - count
				union = float(count + excl1 + excl2)
				if not union:
					union = 1  # ensure that ratios become 0
				count_ratio = count / union # jaccard index
				excl1_ratio = excl1 / union
				excl2_ratio = excl2 / union
				yield (
					sample1, sample2,
					count, count_ratio,
					excl1, excl1_ratio,
					excl2, excl2_ratio
				)
		return render_template('sample-overlap-result.html', quality=quality, patients=patients, stats=stats)

	else:
		return render_template('error.html', message="Wrong Number of samples specified")

