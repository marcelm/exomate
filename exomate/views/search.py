"""
The search page
"""

from flask import render_template
from exomate.application import app


@app.route('/search')
def search():
	"""Show the search mask"""
	return render_template('search.html')


