"""
The diseases page
"""

from exomate.database import get_session
from flask import render_template
from exomate.application import app
from exomate.models import Sample, Disease
from sqlalchemy import func, desc

session = get_session()

@app.route('/diseases')
def diseases():
	grouped_counts = session.query(Sample.disease_id,
		func.count().label("count")).\
		group_by(Sample.disease_id).subquery()
	counts = session.query(Disease, "count").join(grouped_counts, grouped_counts.c.disease_id == Disease.id).order_by(desc("count"))
	return render_template('diseases.html', counts=counts)

