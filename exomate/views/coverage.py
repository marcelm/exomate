"""
The coverage page
"""

from exomate.database import get_session
from flask import render_template
from exomate.application import app
from exomate.models import Call, Sample
from sqlalchemy import func

session = get_session()

@app.route('/coverage')
def coverage():
	"""Display variant coverage"""
	rows = session.query(Call.sample_id, Sample.accession, func.avg(Call.read_depth).label('avg_coverage'),
		func.count(Call.variant_id).label('no_of_calls')).join(Sample).group_by(Call.sample_id, Sample.accession).all()
	return render_template('coverage.html', rows=rows)
