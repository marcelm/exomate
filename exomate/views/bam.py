"""
The index page
"""


import os
import re
from glob import glob
from flask import render_template, request, send_file, Response
from exomate.application import app
from exomate.views.common import get_bam_path
from collections import namedtuple
from datetime import datetime


@app.route('/bams')
def bams():
	bam_path = get_bam_path()
	if not bam_path:
		return render_template('error.html', message="Could not retrieve Bam path")
	os.chdir(bam_path)

	File = namedtuple("File", "name modified size")
	def build(accession):
		bam_path = get_bam_path()
		if not bam_path:
			return render_template('error.html', message="Could not retrieve Bam path")
		stat = os.stat(os.path.join(bam_path, accession))
		modified = datetime.fromtimestamp(stat.st_mtime).strftime('%d-%b-%Y %H:%M')
		size = sizeof_fmt(stat.st_size)
		return File(accession, modified, size)

	files = [build(accession) for accession in glob("*.bam")]
	return render_template("bams.html", files=files)


@app.route('/bam/<accession>')
def get_bam(accession):
	if accession.endswith(".bai"):
		accession = accession.rstrip(".bai").rstrip(".bam") + ".bam.bai"
	bam_path = get_bam_path()
	if not bam_path:
		return render_template('error.html', message="Could not retrieve Bam path")
	return send_file_partial(os.path.join(bam_path, accession))


def sizeof_fmt(num):
	'''return human readable filesizes'''
	for x in ['bytes','KB','MB','GB','TB']:
		if num < 1024.0:
			return "%3.1f %s" % (num, x)
		num /= 1024.0


def send_file_partial(path, mimetype="application/octet-stream"):
    """
    Simple wrapper around send_file which handles HTTP 206 Partial Content
    (byte ranges)
    Taken from http://blog.asgaard.co.uk/t/flask
    TODO: handle all send_file args, mirror send_file's error handling
    (if it has any)
    """

    range_header = request.headers.get('Range', None)

    if not range_header:
        return send_file(path, mimetype=mimetype)

    size = os.path.getsize(path)
    byte1, byte2 = 0, None

    m = re.search('(\d+)-(\d*)', range_header)
    g = m.groups()

    if g[0]: byte1 = int(g[0])
    if g[1]: byte2 = int(g[1]) + 1

    byte2 = min(size, byte2)

    length = size - byte1
    if byte2 is not None:
        length = byte2 - byte1

    data = None
    with open(path, 'rb') as f:
        f.seek(byte1)
        data = f.read(length)

    rv = Response(data,
        206,
        mimetype=mimetype,
        direct_passthrough=True)

    rv.headers.add('Content-Range', 'bytes {0}-{1}/{2}'.format(byte1, byte1 + length - 1, size))

    return rv
