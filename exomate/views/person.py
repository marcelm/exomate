"""
The person page
"""

from flask import render_template, request
from exomate.application import app
from exomate.models import Patient


@app.route('/person')
def persons():
	optgender = request.args.get('gender', "")
	optminphenoscore = request.args.get('minphenoscore', "")
	optmaxphenoscore = request.args.get('maxphenoscore', "")
	optaccession = request.args.get('accession', "")
	optcondition = request.args.get('condition', "")
	optmother = request.args.get('mother', "")
	optfather = request.args.get('father', "")

	patients = Patient.query

	if optgender != "":
		if optgender == "unknown":
			optgender = None
		patients = patients.filter(Patient.gender == optgender)

	if optminphenoscore != "":
		patients = patients.filter(Patient.phenoscore >= optminphenoscore)

	if optmaxphenoscore != "":
		patients = patients.filter(Patient.phenoscore <= optmaxphenoscore)

	if optaccession != "":
		patients = patients.filter(Patient.accession == optaccession)

	if optcondition != "":
		patients = patients.filter(Patient.condition == optcondition)

	if optmother != "":
		patients = patients.filter(Patient.mother.has(Patient.accession == optmother))

	if optfather != "":
		patients = patients.filter(Patient.father.has(Patient.accession == optfather))

	return render_template('persons.html', persons=list(patients))

