"""
The units page
"""

from exomate.database import get_session
from flask import render_template
from exomate.application import app
from exomate.models import Unit, UnitStats, Run, Patient, MappingCount
from sqlalchemy import desc, or_

session = get_session()


@app.route('/units')
def units():
	units = Unit.query.order_by(desc(Unit.run_id).nullslast()).all()
	return render_template('units.html', units=list(units))


@app.route('/unit/<prefix>')
def unit(prefix):
	unitstat = session.query(UnitStats).join(Unit).outerjoin(Run).filter(Unit.prefix == prefix).one()
	patient = unitstat.unit.library.sample.patient
	children = session.query(Patient).filter(or_(Patient.mother.has(Patient.id == patient.id), Patient.father.has(Patient.id == patient.id))).all()
	siblings = set()
	if patient.mother is not None:
		siblings = siblings.union(session.query(Patient).filter(Patient.mother.has(Patient.id == patient.mother.id)).filter(Patient.id != patient.id).all())
	if patient.father is not None:
		siblings = siblings.union(session.query(Patient).filter(Patient.father.has(Patient.id == patient.father.id)).filter(Patient.id != patient.id).all())
	mappingcounts = list(session.query(MappingCount).filter(MappingCount.unit.has(Unit.prefix == prefix)))
	alternative_mapped = 0
	alternative_unmapped = 0
	alternative_exists = False
	total_mapped_reads=0
	for count in mappingcounts:
		total_mapped_reads += count.mapped
		if count.chrom.startswith("GL"):
			alternative_exists = True
			alternative_mapped += count.mapped
			alternative_unmapped += count.unmapped
	if alternative_exists:
		alternative_count = MappingCount(None, "alternative loci", alternative_mapped, alternative_unmapped)
		mappingcounts.append(alternative_count)
	return render_template("unit.html", mappingcounts=mappingcounts, prefix=prefix, unitstat=unitstat, total_mapped_reads=total_mapped_reads, children=children, patient=patient, siblings=siblings)
