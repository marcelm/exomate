"""
The context page
"""

from exomate.database import get_session
from flask import render_template, request
from exomate.application import app
from exomate.models import Sample, Call, Variant, Disease
from exomate.views.common import parse_aux_filter_parameters, get_aux_filter
from collections import defaultdict, OrderedDict
from sqlalchemy import func

session = get_session()

@app.route('/context', methods=['GET', 'POST'])
def context():
	samples = session.query(Sample)
	diseases = session.query(Disease)
	quality = request.args.get('quality', 200, type=int)
	if request.method != "POST":
		return render_template("variant-context-query.html", quality=quality,
			samples=samples, diseases=diseases)

	selected_disease_id = request.form.getlist('diseases', type=int)[0]

	if selected_disease_id == -1:
		# extract samples from post parameters
		affected_sample_ids = request.form.getlist('samples', type=int)
	else:
		# select samples with selected disease_id
		affected_sample_ids = session.query(Sample.id).filter(Sample.disease_id == selected_disease_id)

	affected_samples = session.query(Sample).filter(Sample.id.in_(affected_sample_ids)).all()

	# generate quality filter and aux filter from post parameters
	qual_filter = Call.qual >= quality
	aux_filter_query = parse_aux_filter_parameters(request.form)
	aux_filter = get_aux_filter(aux_filter_query)

	temp = {}
	result = dict()
	sums = defaultdict(int)

	for sample in affected_samples:
		temp[sample] = session.query(
			Variant.context.label("context"),
			func.count(Variant.context).label("count")
		).join(Call).\
			filter(Call.sample_id == sample.id).\
			filter(Variant.context != "").\
			filter(qual_filter).\
			filter(aux_filter).\
			group_by(Variant.context).all()
		for row in temp[sample]:
			sums[sample] += row.count
			if not row.context in result:
				result[row.context] = {}
			result[row.context][sample] = row.count

	return render_template('variant-context-result.html',
		result=OrderedDict(sorted(result.items())), sums=sums,
		samples=affected_samples)

