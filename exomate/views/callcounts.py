"""
The callcounts page
"""

from exomate.database import get_session
from flask import render_template, request
from exomate.application import app
from exomate.models import Sample, Disease, SampleGroup, Call, Variant
from sqlalchemy import func, sql, Integer, distinct
from exomate.views.common import parse_samples, parse_aux_filter_parameters, get_aux_filter

session = get_session()


@app.route('/callcounts', methods=['GET', 'POST'])
def call_counts():
	quality = request.form.get('quality', 50, type=int)
	if request.method != "POST":
		samples = session.query(Sample).join(Disease).order_by(Disease.name, Sample.accession).all()
		sample_groups = session.query(SampleGroup).all()
		return render_template("call-counts-query.html", quality=quality, samples=samples, sample_groups=sample_groups)

	sample_accessions = parse_samples(request.form)
	sample_filter = Sample.accession.in_(sample_accessions)

	qual_filter = Call.qual >= quality
	aux_filter_query = parse_aux_filter_parameters(request.form)
	aux_filter = get_aux_filter(aux_filter_query)

	grouped_counts = session.query(
		Call.sample_id,
		func.count(Call.sample_id).label("count"),
		func.sum(sql.expression.cast(Variant.is_transition, Integer)).label("transitions"),
		func.sum(sql.expression.cast(Variant.is_transversion, Integer)).label("transversions")).\
		filter(qual_filter).group_by(Call.sample_id).\
		join(Variant).filter(aux_filter).subquery()
	counts = session.query(Sample, "count", "transitions", "transversions").outerjoin(grouped_counts, grouped_counts.c.sample_id == Sample.id).filter(sample_filter)

	variant_count, = session.query(func.count(distinct(Variant.id))).join(Call).join(Sample).filter(sample_filter).filter(qual_filter).filter(aux_filter).one()

	#pprint(session.execute(Explain(variant_count)).fetchall())

	return render_template('call-counts-result.html', counts=counts, quality=quality, estimate_fdr=estimate_fdr, calc_titv_rate=calc_titv_rate, variant_count=variant_count)


def estimate_fdr(transitions, transversions):
	'''
	Estimate the FDR based on transition / transversion ratios as it is
	suggested by DePristo et al., Nature Genetics 2011.
	'''
	titv_obs = calc_titv_rate(transitions, transversions)
	if titv_obs is None:
		return None
	titv_exp = 3.2  # expected rate for new variants as stated by DePristo et al. 2011 (set to 3.2 to comply to GATK)
	# estimate should be bounded within 0.1% and 100%
	fdr_estimate = min(1, (max(0.001, 1 - (titv_obs + 0.5) / (titv_exp + 0.5))))
	return fdr_estimate


def calc_titv_rate(transitions, transversions):
	if transitions is None or transversions is None:
		return None
	if transversions == 0:
		return None
	return transitions / transversions



