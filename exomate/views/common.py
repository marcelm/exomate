"""
Helper classes and functions
"""

import itertools
import functools
import operator
import os
from exomate import configuration
from collections import namedtuple

import numpy as np
from scipy.stats import t
import numpy.ma as ma
from functools import reduce

##### EXPLAIN sqlalchemy queries ######
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.expression import Executable, ClauseElement, _literal_as_text

from exomate.database import get_session
from sqlalchemy import func, or_, not_, and_, distinct
from exomate.models import *

session = get_session()

# This constant, when passed to SQLalchemy's .filter() function, means that
# /nothing/ is filtered. It can be used to build filters incrementally.
NO_FILTER = ''

Record = namedtuple("Record", "gene bam sample annotation is_heterozygous quality strand_bias transcripts")

PurityRow = namedtuple("PurityRow", "sample healthy_sample purity std outlier_count")


class Explain(Executable, ClauseElement):
	def __init__(self, stmt, analyze=False):
		self.statement = _literal_as_text(stmt)
		print(self.statement, file.stderr)
		self.analyze = analyze


@compiles(Explain, 'postgresql')
def pg_explain(element, compiler, **kw):
	text = "EXPLAIN "
	if element.analyze:
		text += "ANALYZE "
	text += compiler.process(element.statement)
	return text


class Row:
	def __init__(self):
		self.values = []

	def add(self, call_annotation):
		self.values += [call_annotation]

	@property
	def gene(self):
		return self.values[0].annotation.transcript.gene

	@property
	def samples(self):
		return set(call.sample for call, annotation, bed in self.values)

	@property
	def transcripts(self):
		return set(annotation.transcript for call, annotation, bed in self.values)

	@property
	def sortedfeatures(self):
		features = list({annotation.feature.transcript:annotation.feature for call, annotation, bed in self.values}.values())
		return sorted(features, key=lambda x: x.transcript.name)

	@property
	def ref(self):
		return self.values[0].annotation.variant.ref

	@property
	def alt(self):
		return self.values[0].annotation.variant.alt

	@property
	def ref_codon(self):
		return self.values[0].annotation.ref_codon

	@property
	def alt_codon(self):
		return self.values[0].annotation.alt_codon

	@property
	def ref_acid(self):
		if self.values[0].annotation.hgvsp:
			return self.values[0].annotation.hgvsp[-10]
		return ""

	@property
	def alt_acid(self):
		if self.values[0].annotation.hgvsp:
			return self.values[0].annotation.hgvsp[-8]
		return ""

	@property
	def is_heterozygous(self):
		return set(call.is_heterozygous for call, annotation, bed in self.values)

	@property
	def splice_dist(self):
		return self.values[0].annotation.splice_dist

	@property
	def qualities(self):
		return set(call.qual for call, annotation, bed in self.values)

	@property
	def minimum_quality(self):
		return min(self.qualities)

	@property
	def maximum_quality(self):
		return max(self.qualities)

	@property
	def consequence(self):
		return reduce(lambda x, y: x | y, [annotation.consequence for call, annotation, bed in self.values])

	@property
	def hgvsp(self):
		return set(annotation.hgvsp.replace("%3D", "=") for call, annotation, bed in self.values if not annotation.hgvsp == "")

	@property
	def sift_score(self):
		return self.values[0].annotation.sift_score

	@property
	def polyphen_score(self):
		return self.values[0].annotation.polyphen_score

	@property
	def is_indel(self):
		return not (self.values[0].annotation.variant.is_transition or self.values[0].annotation.variant.is_transversion)

	@property
	def strand_bias(self):
		return set(call.strand_bias for call, annotation, bed in self.values)

	@property
	def read_depth(self):
		return set(call.read_depth for call, annotation, bed in self.values)

	@property
	def ref_depth(self):
		return set(call.ref_depth for call, annotation, bed in self.values)

	@property
	def alt_depth(self):
		return set(call.alt_depth for call, annotation, bed in self.values)

	@property
	def chrom(self):
		return self.values[0].annotation.variant.chrom

	@property
	def pos(self):
		return self.values[0].annotation.variant.pos

	@property
	def bams(self):
		units = [unit for call, annotation, bed in self.values for unit in call.sample.units]
		return sorted(set(units))

	@property
	def bednames(self):
		return set(bed.name for call, annotation, bed in self.values)

	@property
	def bedscores(self):
		return set(bed.score for call, annotation, bed in self.values)

	@property
	def eur_maf(self):
		return self.values[0].annotation.eur_maf

	@property
	def afr_maf(self):
		return self.values[0].annotation.afr_maf

	@property
	def amr_maf(self):
		return self.values[0].annotation.amr_maf

	@property
	def asn_maf(self):
		return self.values[0].annotation.asn_maf

	@property
	def exon_number(self):
		return self.values[0].annotation.exon_number

	@property
	def exon_count(self):
		return self.values[0].annotation.exon_count


def get_incomplete_annotation_warning():
	max_annotated_id = session.query(func.max(AnnotationRun.max_variant_id)).scalar()
	max_variant_id = session.query(func.max(Variant.id)).scalar()
	if not (max_annotated_id and max_variant_id):
		message = "Could not retrieve data. Database possibly empty"
	elif max_annotated_id < max_variant_id:
		message = "Some variants have not been annotated! Results will likely be incomplete."
	else:
		message = None
	return message


def generalizedESD(x, maxOLs, alpha=0.05, fullOutput=False):
	"""
	Modified Version taken from PyAstronomy outlier.py, provided under the MIT license.
	https://github.com/sczesla/PyAstronomy/blob/master/src/pyasl/asl/outlier.py

	Carry out a Generalized ESD Test for Outliers.

	The Generalized Extreme Studentized Deviate (ESD) test for
	outliers can be used to search for outliers in a univariate
	data set, which approximately follows a normal distribution.
	A description of the algorithm is, e.g., given at
	`Nist <http://www.itl.nist.gov/div898/handbook/eda/section3/eda35h3.htm>`_
	or [Rosner1983]_.

	Parameters
	----------
	maxOLs : int
		Maximum number of outliers in the data set.
	alpha : float, optional
		Significance (default is 0.05).
	fullOutput : boolean, optional
		Determines whether additional return values
		are provided. Default is False.

	Returns
	-------
	Number of outliers : int
		The number of data points characterized as
		outliers by the test.
	Indices : list of ints
		The indices of the data points found to
		be outliers.
	R : list of floats, optional
		The values of the "R statistics". Only provided
		if `fullOutput` is set to True.
	L : list of floats, optional
		The lambda values needed to test whether a point
		should be regarded an outlier. Only provided
		if `fullOutput` is set to True.
	"""
	if maxOLs < 1:
		return render_template('error.html', message=" ".join("Maximum number of outliers, `maxOLs`, must be > 1."))
	xm = ma.array(x)
	n = len(xm)
	# Compute R-values
	R = []
	L = []
	minds = []
	for i in range(maxOLs + 1):
		# Compute mean and std of x
		xmean = xm.mean()
		xstd = xm.std()
		# Find maximum deviation
		rr = np.abs((xm - xmean)/xstd)
		minds.append(np.argmax(rr))
		R.append(rr[minds[-1]])
		if i >= 1:
			p = 1.0 - alpha/(2.0*(n - i + 1))
			perPoint = t.ppf(p, n-i-1)
			L.append((n-i)*perPoint / np.sqrt((n-i-1+perPoint**2) * (n-i+1)))
		# Mask that value and proceed
		xm[minds[-1]] = ma.masked
	# Remove the first entry from R, which is of
	# no meaning for the test
	R.pop(-1)
	# Find the number of outliers
	ofound = False
	for i in range(maxOLs-1, -1, -1):
		if R[i] > L[i]:
			ofound = True
			break
	# Prepare return value
	if ofound:
		if not fullOutput:
			# There are outliers
			return i+1, minds[0:i+1]
		else:
			return i+1, minds[0:i+1], R, L, minds
	else:
		# No outliers could be detected
		if not fullOutput:
			# There are outliers
			return 0, []
		else:
			return 0, [], R, L, minds


class QueryParameters:
	pass


def parse_samples(form):
	sample_names = form.getlist('samples')
	samplegroup_ids = form.getlist('sample_groups', type=int)
	disease_ids = form.getlist('diseases')
	samplegroups = session.query(SampleGroup).filter(SampleGroup.id.in_(samplegroup_ids)).all() if samplegroup_ids else []
	for group in samplegroups:
		for sample in group.samples:
			sample_names.append(sample.accession)
	diseases = session.query(Disease).filter(Disease.id.in_(disease_ids)).all() if disease_ids else []
#	print(diseases, disease_ids, file=sys.stderr)
	for disease in diseases:
		for sample in disease.samples:
			sample_names.append(sample.accession)
	return sample_names


def parse_aux_filter_parameters(form):
	q = QueryParameters()
	q.names = ('strand_bias', 'qual_by_depth', 'mapping_qual', 'haplotype_score', 'mapping_qual_bias', 'read_pos_bias')
	q.operators = (operator.le, operator.ge, operator.ge, operator.le, operator.ge, operator.ge)
	q.get_form_name = '{}_{}'.format
	for prefix in ('snp', 'indel'):
		for name in q.names:
			name = q.get_form_name(prefix, name)
			setattr(q, name, form.get(name, type=float))
	return q


def get_aux_filter(aux_filter_query):
	aux_filters = lambda prefix: [(getattr(Call, f), getattr(aux_filter_query, aux_filter_query.get_form_name(prefix, f))) for f in aux_filter_query.names]
	def get_aux_filters(prefix):
		for (call_attr, threshold), op in zip(aux_filters(prefix), aux_filter_query.operators):
			if threshold is not None:
				yield or_(call_attr == None, op(call_attr, threshold))

	is_snp = or_(Variant.is_transition, Variant.is_transversion)
	aux_filter_snps = is_snp
	aux_filter_indels = not_(is_snp)
	aux_filter_snps &= and_(*get_aux_filters('snp'))
	aux_filter_indels &= and_(*get_aux_filters('indel'))
	# TODO remove the following line once both columns cannot be None by definition.
	aux_filter_empty = or_(Variant.is_transition == None, Variant.is_transversion == None)
	aux_filter = or_(aux_filter_empty, aux_filter_snps, aux_filter_indels)
	return aux_filter


def parse_mutation_query_parameters(form):
	q = QueryParameters()
	filter_diseases_id = form.getlist('filter_diseases', type=int)
	q.filter_diseases = session.query(Disease).filter(Disease.id.in_(filter_diseases_id)).all() if filter_diseases_id else []
	filter_samples_ids = form.getlist('filter_samples', type=int)
	filter_samples = session.query(Sample).filter(Sample.id.in_(filter_samples_ids)).all() if filter_samples_ids else []
	filter_samplegroup_ids = form.getlist('filter_sample_groups', type=int)
	filter_samplegroup = session.query(SampleGroup).filter(SampleGroup.id.in_(filter_samplegroup_ids)).all() if filter_samplegroup_ids else []
	q.consequences = form.getlist('consequences', type=int)
	for group in filter_samplegroup:
		for sample in group.samples:
			filter_samples.append(sample)
	q.filter_samples = filter_samples
	q.ignore_heterozygous = ('ignore_heterozygous' in form)
	q.show_dbsnp_clinical = ('clinical' in form)
	q.show_dbsnp_precious = ('precious' in form)
	q.show_dbsnp_locus_specific = ('locus-specific-db' in form)
	q.show_coding_variants = ('codingvariants' in form)
	q.show_utr_variants = ('utrvariants' in form)
	q.show_intron_variants =  ('intronvariants' in form)
	q.show_protein_products_only = ('products-only' in form)
	q.all_genes = ('all_genes' in form)
	q.affected_sample_names = parse_samples(form)
	q.affected_qual = form.get('affected_qual', 0, type=int)
	q.unaffected_qual = form.get('unaffected_qual', 0, type=int)
	q.splice_dist = form.get('splice_dist', 10, type=int)
	q.recessive = form.get('recessive')
	q.is_recessive = form.get('recessive') != "no"
	q.gene_names = form.get('genes', '').split()
	q.show_heterozygous = form.get('heterozygous') == "on"
	q.show_homozygous = form.get('homozygous') == "on"
	q.min_samples_per_gene = form.get('min_samples_per_gene', 0, type=int)
	q.min_variants_per_gene = form.get('min_variants_per_gene', 0, type=int)
	q.min_samples_per_variant = form.get('min_samples_per_variant', 0, type=int)
	q.bedfile_accession = form.get('bedfiles')
	q.consequences_bits = reduce(lambda x,y: x|2**y, q.consequences, 0)
	q.min_eur_maf = form.get('min_eur_maf', 0.0, type=float)
	q.min_amr_maf = form.get('min_amr_maf', 0.0, type=float)
	q.min_afr_maf = form.get('min_afr_maf', 0.0, type=float)
	q.min_asn_maf = form.get('min_asn_maf', 0.0, type=float)
	q.max_eur_maf = form.get('max_eur_maf', 1.0, type=float)
	q.max_amr_maf = form.get('max_amr_maf', 1.0, type=float)
	q.max_afr_maf = form.get('max_afr_maf', 1.0, type=float)
	q.max_asn_maf = form.get('max_asn_maf', 1.0, type=float)

#	recessive
	q.ancestor1 = form.get('ancestor1')
	q.ancestor2 = form.get('ancestor2')
	q.ks = form.getlist('filter_known', type=int)
	q.affected_sample_name = form.get('samples')
	q.query_mode = form.get('hom_or_het', "")
	q.allelecount_inhouse = form.get('allelecount_inhouse', 0, type=int)
	q.dbsnp_allelefreq = form.get('allelefreq', 0.0, type=float)

#	q.splice_dist = form.get('splice_dist', 10, type=int)
#	q.show_coding_variants = ('codingvariants' in form)
#	q.show_utr_variants = ('utrvariants' in form)
#	q.show_intron_variants =  ('intronvariants' in form)

#	q.affected_qual = form.get('affected_qual', 0, type=int)
#	q.unaffected_qual = form.get('unaffected_qual', 0, type=int)
#	q.all_genes = True


	ks = form.getlist('filter_known', type=int)
	q.known_sources = session.query(KnownSource).filter(KnownSource.id.in_(ks)).all() if ks else []
	return q


def get_pileup(sample, chrom, pos, bam_path=None):
	assert bam_path is not None
	for unit in sample.units:
		with pysam.Samfile(os.path.join(bam_path, unit.prefix + ".bam"), "rb") as samfile:
			cols = samfile.pileup(chrom.encode(), pos, pos+1)

			for col in cols:
				if col.pos != pos:
					continue
				for read in col.pileups:
					aln = read.alignment
					if aln.mapq != 0:
						base = chr(aln.seq[read.qpos])
						# calculate phred-scaled base quality from ascii in SAM
						qual = aln.qual[read.qpos] - 33
						yield base, qual, aln.is_reverse, read.is_del


def get_bam_path():
	try:
		return os.path.join(os.path.dirname(configuration.get_path()), configuration.parse().get("pipeline", "bam_path"))
	except Exception:
		return None

