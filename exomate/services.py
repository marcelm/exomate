import urllib.request, urllib.parse, urllib.error
from xml.dom.minidom import parseString

#Biopython currently not in use
#from Bio import Entrez

#Entrez.email = "christina.czeschik@uni-due.de"
#(will be notified when Entrez's query limit is exceeded before blocking the IP)

#def get_omim(gene_name):
#    handle = Entrez.esearch(db="omim", term=gene_name)
#    record = Entrez.read(handle)
#    return record["IdList"]


def get_omim(gene_name):
	esearch_url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=omim&term="
	omim_site = urllib.request.urlopen(esearch_url + gene_name)
	omim_text = omim_site.read()
	omim_site.close()
	omim_xml = parseString(omim_text)
	omim_list = []
	for omim_nr in omim_xml.getElementsByTagName("Id"):
		for t in omim_nr.childNodes:
			if t.nodeType == t.TEXT_NODE:
				omim_list.append(t.nodeValue)
	return omim_list


def get_omim_str(gene_name):
	return ", ".join(get_omim(gene_name))


def get_haploins_score(gene_name):
	return -1
	haplo_str = "<li>Haploinsufficiency score: "
	isca_url = "http://www.ncbi.nlm.nih.gov/projects/dbvar/ISCA/isca_gene.cgi?sym="
	isca_site = urllib.request.urlopen(isca_url + gene_name)
	isca_text = isca_site.read()
	isca_site.close()
	if haplo_str in isca_text:
		haplo_score_pos = isca_text.find(haplo_str) + len(haplo_str)
		haplo_score_end = isca_text.find("</li>", haplo_score_pos)
		return isca_text[haplo_score_pos:haplo_score_end]
	return None


def get_triplosens_score(gene_name):
	return -1
	triplo_str = "<li>Triplosensitivity score: "
	isca_url = "http://www.ncbi.nlm.nih.gov/projects/dbvar/ISCA/isca_gene.cgi?sym="
	isca_site = urllib.request.urlopen(isca_url + gene_name)
	isca_text = isca_site.read()
	isca_site.close()
	if triplo_str in isca_text:
		triplo_score_pos = isca_text.find(triplo_str) + len(triplo_str)
		triplo_score_end = isca_text.find("</li>", triplo_score_pos)
		return isca_text[triplo_score_pos:triplo_score_end]
	return None
