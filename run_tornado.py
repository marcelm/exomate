#!/usr/bin/env python3

from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from exomate.application import app
from exomate import configuration

port = int(configuration.parse().get("webserver", "port"))

http_server = HTTPServer(WSGIContainer(app))
http_server.listen(port)
IOLoop.instance().start()

