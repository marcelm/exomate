from distutils.core import setup, Extension, Command
import sys
import textwrap
import subprocess
from glob import glob
import os

__version__ = '0.1'

if sys.version < '3.2':
	sys.stdout.write("At least Python 3.2 is required (try running with 'python3 setup.py').\n")
	sys.exit(1)

class InstallServer(Command):
	description = "Install the Exomate server into the given directory"
	user_options = [("prefix=", None, "The prefix to install to.")]

	def initialize_options(self):
		"""Use this to set option defaults before parsing."""
		self.prefix = None
 
	def finalize_options(self):
		"""Code to validate/modify command-line/config input goes here."""
		if self.prefix is None:
			raise DistutilsOptionError('"prefix" is required')
		if not os.path.exists(self.prefix):
			raise DistutilsOptionError('"prefix" has to be a valid path')
 
	def run(self):
		"""Your actual command functionality goes here."""
		subprocess.check_call(textwrap.dedent("""
		rsync -vrlD --exclude '*.pyc' \\
		        --exclude '*~' \\
		        --exclude '*.cfg' \\
		        --exclude '__pycache__' \\
		        ./exomate/ {prefix}/exomate/
		rsync -p \\
		        ./run_tornado.py {prefix}/
		echo "git_revision='$(date +"%Y-%m-%d") $(git rev-parse --short HEAD)'" >> {prefix}/exomate/__init__.py
		chmod -R o+r {prefix}
		""".format(prefix=self.prefix)), shell=True)


setup(
	name = 'exomate',
	version = __version__,
	author = 'Marcel Martin',
	author_email = 'marcel.martin@tu-dortmund.de',
	url = '',
	description = '',
	license = 'MIT',
	cmdclass={"install_server": InstallServer},
	packages = ['exomate', 'exomate.scripts'],
	scripts = [ s for s in glob(os.path.join("bin", "exomate-*")) if not s.endswith('~') ],
	install_requires = [
		"SQLalchemy >= 0.8.0b2",
		"Flask-SQLAlchemy"
	],
	classifiers = [
		"Development Status :: 4 - Beta",
		#"Development Status :: 5 - Production/Stable",
		"Environment :: Console",
		"Intended Audience :: Science/Research",
		"License :: OSI Approved :: MIT License",
		"Natural Language :: English",
		"Programming Language :: Python :: 3",
		"Topic :: Scientific/Engineering :: Bio-Informatics"
	]
)
