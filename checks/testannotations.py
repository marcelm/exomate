from exomate.database import get_session
from exomate.variants import VARIANT_TYPES, VARIANT_REGIONS

session = get_session()

def test_utr_annotation():
	"""
	Test that a UTR annotated variant is in no coding region of the same transcript
	(empty if correct)
	"""
	result = session.execute("""
	    SELECT f.type, f.id as fid, v.id as vid, v.pos, f.start, f.stop from features f
	    JOIN variants v on v.pos BETWEEN f.start AND (f.stop-1)
	    JOIN annotations a on a.variant_id = v.id AND a.transcript_id = f.transcript_id
	    WHERE a.region=:r AND f.type!='exon'"""
	,{"r":VARIANT_REGIONS.UTR})
	assert result.fetchone() is None


def test_type_annotation():
	"""
	Test the Type Annotations
	Possibly not all cases covered
	(empty if correct)
	"""
	result = session.execute("""
	    SELECT * from annotations
	    WHERE (type = :t1 AND (length(ref_codon) > 0 OR
	      length(alt_codon) > 0 OR
	      length(ref_acid) > 0 OR
	      length(alt_acid) > 0)) OR
	      (type = :t2 AND ref_acid != '' AND ref_acid != alt_acid) OR
	      (ref_acid != '' AND ref_acid = alt_acid AND type != :t2) OR
	      (type = :t3 AND alt_acid != '*') OR
	      (type = :t4 AND (length(alt_codon) = 0 OR length(ref_codon) = 0 OR length(alt_codon) = 0 OR length(ref_codon) = 0)) OR
	      (ref_acid != alt_acid AND alt_acid != '*' AND ref_acid != '*' AND type != :t4) OR
	      (alt_acid = '*' AND ref_acid != '*' AND type != :t3) OR
	      (type = :t5 AND ref_acid != '*') OR
	      (ref_acid = '*' AND alt_acid != '*' AND type != :t5)"""
	,{"t1":VARIANT_TYPES.DELETION, "t2":VARIANT_TYPES.SYNONYMOUS,
	  "t3":VARIANT_TYPES.NONSENSE, "t4":VARIANT_TYPES.MISSENSE, "t5":VARIANT_TYPES.READTHROUGH})
	assert result.fetchone() is None


def test_all_annotation_hit_feature():
	"""
	Test that all annotations hitting the features
	(empty if correct)
	"""    
	result = session.execute("""
	    SELECT variant_id from annotations
	    EXCEPT
	    SELECT v.id from variants v
	    JOIN features f on v.pos BETWEEN f.start-10 AND f.stop+9 AND v.chrom = f.chrom""")
	assert result.fetchone() is None
    

def test_hitting_features_are_annotated():
	"""
	Test that all hitting features are annotated
	(
	on perfekt Data: 0 ,reality: some (current 16)
	cause there are exons without correct end or without correct start
	)
	(correct if below 50) 
	""" 	       
	resultproxy = session.execute("""
	    SELECT v.id from variants v
	    JOIN features f on v.pos BETWEEN f.start-10 AND f.stop+9 AND v.chrom = f.chrom 
	    WHERE f.source='protein_coding'
	    EXCEPT
	    SELECT variant_id from annotations""")
	result = 0
	for row in resultproxy:
		result = result + 1
	assert result <= 50


def test_no_intron_on_exon():
	"""
	Test that no variant marked as intron is on a codon
	(empty if correct)
	"""        
	result = session.execute("""
	    SELECT variant_id, transcript_id FROM annotations
	    WHERE region=:r
	    INTERSECT
	    SELECT v.id, f.transcript_id FROM variants v 
	    JOIN features f ON v.pos BETWEEN f.start AND f.stop - 1 AND v.chrom = f.chrom"""
	,{"r":VARIANT_REGIONS.INTRON})
	assert result.fetchone() is None


def test_all_codinganotated_not_in_splice():
	"""
	Test that every variant marked as coding is not in splice
	(empty if correct)
	"""        
	result = session.execute("""
	    SELECT variant_id, transcript_id from annotations
	    WHERE region =:r
	    EXCEPT
	    SELECT v.id, f.transcript_id from variants v
	    JOIN features f on v.pos BETWEEN f.start AND f.stop - 1 AND v.chrom = f.chrom
	    WHERE f.type='CDS' OR f.type='stop_codon' OR f.type ='start_codon'"""
	,{"r":VARIANT_REGIONS.CODING})
	assert result.fetchone() is None


def test_all_introns_in_or_near_splice():
	"""
	Test that every variant annotated as intron is in an area of 10 bases from a coding area
	(empty if correct)
	"""
	result = session.execute("""
	    SELECT variant_id, transcript_id FROM annotations
	    WHERE region = :r
	    EXCEPT
	    SELECT v.id, f.transcript_id FROM variants v 
	    JOIN features f 
	    ON (v.pos BETWEEN f.start-10 AND f.start - 1 OR v.pos BETWEEN f.stop AND f.stop+9) AND v.chrom = f.chrom"""
	,{"r":VARIANT_REGIONS.INTRON})
	assert result.fetchone() is None
