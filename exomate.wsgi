import site
import os

# these lines are necessary for virtualenvs
#activate_this = '/path/to/env/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))

import sys, logging
logging.basicConfig(stream = sys.stderr)

site.addsitedir('/home2/exom/exomate')
os.environ['EXOMATE_CONFIG'] = '/home2/exom/exomate/exomate.conf'
from exomate.application import app as application
