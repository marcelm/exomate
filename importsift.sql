.nullvalue NULL
select "BEGIN;";
.mode insert sift_ensg
select * from sift_ensg;
.mode insert sift_enst
select * from sift_enst;
.mode insert sift_regions
select * from sift_regions;
.mode insert sift
select
	chrom, pos, strand, enst_id, region_id, nt1, nt2, ntpos1, ntpos2, aa1, aa2,
	score, median, seqs_rep
from sift where cds = 1;
.mode list
select "END;";
