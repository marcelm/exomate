#!/bin/bash
set -e

function usage() {
	echo "covplot [--title TITLE] <OUTPUT-PDF> [<HIST>]"
	echo
	echo "Read a file created with 'bedtools coverage -hist' from HIST or from standard input."
	echo "Write a plot as PDF."
	echo
	exit 2
}

function catorzcat() {
	if [ "${1##*.}" = "gz" ]; then
		zcat "$1"
	else
		cat "$1"
	fi
}

NAME=""
while [ $# -ge 1 ]; do
	if [ "$1" = "--title" ]; then
		NAME="$2"
		shift 2;
	else
		break
	fi
done

if [ $# -ne 1 -a $# -ne 2 ]; then
	usage "need one or two parameters"
fi

PDF="$1"
if [ $# -eq 2 ]; then
	COVHIST="$2"
else
	COVHIST="/dev/stdin"
fi

if [ x$NAME = x ]; then
	# guess plot name from filename
	FNAME=$(basename $COVHIST)
	NAME=${FNAME%%.cov*}
fi
TABLE=$(mktemp) || exit 1
trap "rm $TABLE" EXIT

if [ $# -eq 2 ]; then
	catorzcat "$COVHIST"
else
	cat
fi |
(
	echo -e "#Coverage\tAt this cov.\tAt least this cov."
	awk '$1 == "all" { print $2 "\t" $5 "\t" 1-x; x+=$5}'
) > $TABLE
cat <<EOF | gnuplot
set term pdf
set output "$PDF"
set grid
set yrange [0:100]
set xrange [0:80]
set title "Coverage $NAME"
set ylabel "Percent of nucleotides"
set xlabel"Minimum coverage"
# set style data boxes
# set style fill solid
unset key
plot "$TABLE" u 1:(\$3*100)
EOF
head -n 22 $TABLE
