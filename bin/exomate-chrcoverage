#!/bin/bash
# Author: Manuel Allhoff

if [ $# -ne 2 ]; then
	echo "Usage:"
	echo
	echo "chrcoverage <BAM> <GFF>"
	echo
	echo "Compute coverage for all targets specified in the GFF file,"
	echo "and output mean coverage per chromosome."
	exit 1
fi

bedtools coverage -abam "$1" -b "$2" -hist | \
	awk -vOFS='\t' '!/^all/ {print $1,$4,$5,$9,$10,$11,$12,($9*$10)}' | \
	bedtools groupby -i stdin -grp 1,2,3,6 -opCols 8 -ops sum | \
	awk -vOFS='\t' '{print $1,$2,$3,$4,$5,$5/$4}' | \
	bedtools groupby -i stdin -grp 1 -opCols 6 -ops mean | \
	grep -v ^GL000 | \
	sort -n
