Exomate
=======

Exomate is an exome-sequencing pipeline with a user-friendly web frontend. It automates most steps needed to go from the sequencing data to variant calls. The web frontend then allows interactive analysis with dynamic filtering.

Parts of the pipeline were originally based on the GATK "Whole exome v1" best-practice document recommendations.

Exomate consists of three conceptual parts:

- The pipeline, which does read mapping up to variant calling and database import
- The database (currently PostgreSQL),
- The web frontend for interactive analyses.

Each part can be run on a different machine: The pipeline on one or more compute servers, the database on a dedicated database machine, and the web frontend on the web server. It is not necessary to do so, however and the installation instructions below do not make this distinction for simplicity.


First part: exomate-pipeline
----------------------------

The variant calling pipeline needs:

- external tools and libraries
- the exomate-specific scripts
- external data (such as dbSNP and the SIFT database)
- the reads in FASTQ format


### Installation on Ubuntu 12.04 LTS


## Install the PostgreSQL database server

    sudo apt-get install postgresql

Create a postgre user named `exomate` (enter a password when prompted):

	sudo -u postgres createuser -P -d -R -S exomate

Create postgresql's password configuration file (replace MY_PASSWORD with the password
you chose above):

    echo '*:*:exomate:exomate:MY_PASSWORD' > ~/.pgpass
    chmod 600 ~/.pgpass

Create a database:

	createdb -U exomate -h localhost exomate

Check that you can connect to Postgresq:

    psql -U exomate -h localhost -c "SELECT 'hello';"
    
There should not be an error message.

Create exomate's database configuration file:

    mkdir -p ~/.config
    echo '[database]' > ~/.config/exomate.conf
    echo 'path=postgresql://exomate@localhost/' >> ~/.config/exomate.conf


## Get exomate from Git

    sudo apt-get install git
    git clone https://bitbucket.org/marcelm/exomate.git
    export EXOMATE=$PWD/exomate

The environment variable is set only to simplify the instructions below. It is not necessary in production use. The pipeline directory will be `$EXOMATE/pipeline/` . You can and should
probabably choose a different directory as lots of space is required.


## Install the pipeline's dependencies

### Python

Python 3.3 or later is required. The instructions below use 3.4 because it comes with pip pre-installed. If you use 3.3, you need to additionally make sure that you have pip installed.

Install everything that is necessary to compile the Python packages that Exomate needs:

    sudo apt-get install build-essential libblas-dev \
        liblapack-dev libatlas-base-dev gfortran postgresql-server-dev-9.1

    sudo apt-get install python-software-properties 
    sudo add-apt-repository ppa:fkrull/deadsnakes
    sudo add-apt-repository ppa:debian-med/ppa
    sudo apt-get update
    sudo apt-get install python3.4 python3.4-dev
    sudo apt-get install bwa samtools picard-tools bedtools gnuplot unzip
 
(the VIRTUAL_ENV var needs to be set: http://stackoverflow.com/questions/15370872/)

    VIRTUAL_ENV=$PWD/venv pyvenv-3.4 venv
    source venv/bin/activate
    python -m ensurepip
    pip install -r requirements.txt
    python setup.py install

(Alternatively, virtualenv can be used.)


### Install FastQC

Download the ZIP file from
<http://www.bioinformatics.bbsrc.ac.uk/projects/download.html#fastqc> .

    wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.10.1.zip
	unzip fastqc_v0.10.1.zip
	chmod +x FastQC/fastqc
	sudo ln -s $PWD/FastQC/fastqc /usr/local/bin


### Install the GATK

Get the [Genome Analysis Toolkit](http://www.broadinstitute.org/gatk/).
Unpack the downloaded tar file and copy `GenomeAnalysisTK.jar` to
`/usr/local/share/gatk/`.

    sudo mkdir -p /usr/local/share/gatk/
    sudo cp GenomeAnalysisTK.jar /usr/local/share/gatk/

Alternatively, copy the jar file to some other location and adjust the path in `$EXOMATE/pipeline/pipeline.config`.

GATK runs only with Oracle's JRE. Follow the instructions at http://stackoverflow.com/a/16263651/715090 to install and enable it.

## External data


### Human genome reference and index

Download your preferred human genome reference and index it with BWA (or copy an index you already have).

    cd $EXOMATE/pipeline
    wget -P reference ftp://ftp.sanger.ac.uk/pub/1000genomes/tk2/main_project_reference/human_g1k_v37.fasta.gz
    gunzip reference/human_g1k_v37.fasta.gz
    bwa index reference/human_g1k_v37.fasta
    samtools faidx reference/human_g1k_v37.fasta

Note that the reference needs to be uncompressed. BWA would be able to deal with a compressed reference, but other tools in the pipeline require an uncompressend reference. (TODO: check this again, at least GATK may now be able to deal with a compressed reference).


### dbSNP

Download and import the current dbSNP:

    cd $EXOMATE/pipeline
    mkdir db
    wget -P db ftp://ftp.ncbi.nih.gov/snp/organisms/human_9606/VCF/common_no_known_medical_impact_20140303.vcf.gz
    exomate-importknown reference/human_g1k_v37.fasta db/common_no_known_medical_impact_20140303.vcf.gz

The last step will take a few hours.


### ENSEMBL


    cd $EXOMATE/pipeline
    wget -P db ftp://ftp.ensembl.org/pub/release-75/gtf/homo_sapiens/Homo_sapiens.GRCh37.75.gtf.gz
    exomate-importensembl db/Homo_sapiens.GRCh37.75.gtf.gz
    gunzip db/Homo_sapiens.GRCh37.75.gtf.gz


### Exome capture kit tracks

need to be put into `$EXOMATE/pipeline/db/capturetracks/` . 

----

NOTE Installation instructions have been revised up to here.

----

# Example data set


## Add metadata (TODO move this)

Run the `exomate-shell` (it is just an interactive Python session):

    exomate-shell

Copy and paste these instructions to add the appropriate meta data for the example dataset:

    healthy = Disease(short_name='healthy', name='healthy')
    um = Disease(short_name='um', name='uveal melanoma')
    
    p2 = Patient(accession='E02')
    p5 = Patient(accession='E05')
    
    ck = CaptureKit(name='NimbleGen Sequence Capture Human Exome 2.1M Array', short_name='NimbleGen 2.1M', path='roche-exome-hg19.gff')
    
    s2b = Sample(accession='E02_blood', patient=p2, disease=healthy, tissue='B')
    s2t = Sample(accession='E02_tumor', patient=p2, disease=um, tissue='T')
    s5b = Sample(accession='E05_blood', patient=p5, disease=healthy, tissue='B')
    s5t = Sample(accession='E05_tumor', patient=p5, disease=um, tissue='T')

    
    l2b = Library(sample=s2b, capturekit=ck)
    l2t = Library(sample=s2t, capturekit=ck)
    l5b = Library(sample=s5b, capturekit=ck)
    l5t = Library(sample=s5t, capturekit=ck)
    u2b = Unit(prefix='E02_blood', library=l2b)
    u2t = Unit(prefix='E02_tumor', library=l2t)
    u5b = Unit(prefix='E05_blood', library=l5b)
    u5t = Unit(prefix='E05_tumor', library=l5t)
    for o in healthy, um, p2, p5, ck, s2b, s2t, s5b, s5t, l2b, l2t, l5b, l5t:
        session.add(o)
    
    session.commit()

Press Ctrl+D to exit.



### Installing Picard Tools

Picard must be callable through the command `picard-tools`. If you installed the
picard-tools package from the Debian Med repository, then this is already
available. Otherwise, create a file named `/usr/local/bin/picard-tools` with this content:

    jar=$1.jar; shift; exec java -Xmx4G -jar /path/to/picard/jar/files/$jar "$@"

Then make the file executable.


### Get external data

These external/3rd-party files are needed by exomate:

- The SIFT database
- Exome capture tracks in GTF, GFF or BED format for the exome capture kits that are used.

Beware! The SIFT database is huge - you will need to download about 50 GB.
Also, BWA indexing takes a few hours.

GFF or BED files should have the following format: 
chr \t startpos \t endpos
with chr = 1, 2, ... (not chr1, chr2, ...)


### Initialize the pipeline

Choose a working directory for Exomate. Since this is where the mapped BAM files
will live, you will need lots of space! We suppose you have chosen `/vol/exomate/` in the
following.

1. Copy the Snakefile and the example configuration file

        cp path/to/Snakefile /path/to/pipeline.config /vol/exomate/Snakefile

2. Edit the `pipeline.config` file.

3. Create a `units.txt` file that describes your FASTQ files. For paired-end data, it has lines of the form

        UNITNAME.1    path/to/raw/data.fastq.1.gz
        UNITNAME.2    path/to/raw/data.fastq.2.gz

    For single-end data, it has lines of the form

	    UNITNAME     path/to/raw/data.fastq.gz

At this point, you should be able to run `snakemake bams/UNITNAME.bam` and get, after a few hours,
a mapped BAM file in the `bams/` directory.

For the remaining steps, the Snakefile needs access to the database, from where it gets
the metadata. Currently, it reads two pieces of information from the database:

- The capture kit that was used for a unit.
- The name of the sample that belongs the a sequenced unit.

After installing the database and putting the appropriate metadata into it, you can then
see what snakemake would do with:

	snakemake -n

Finally, run `snakemake` without the `-n` parameter to actually run everything.
This will

- create the mapped BAM files
- call variants and put them, for each sample, into the `vcf/` directory.
- import variants into the database using `exomate-importcalls`
- create some other statistics files that are also imported into the database
  (with `exomate-importkvs`)


exomate-database: PostgreSQL database server
============================================

On the `exomate-database` machine, you need to install PostgreSQL. Use the
approriate Debian/Ubuntu packages.


### Configure PostgreSQL

In Ubuntu, the PostgreSQL configuration file is in
`/etc/postgresql/8.4/main/postgresql.conf` (where 8.4 is your actual
PostgreSQL version). The following settings should be adjusted.
(Since this is probably not perfect, also ask your local PostgreSQL
database tuning expert.)

- `data_directory`: Make sure that there is enough space on the partition on which your
PostgreSQL data directory is located. If there isn't, change `data_directory` and
initialize the directory by running:

	/usr/lib/postgresql/8.4/bin/initdb -D /path/to/postgres/data

- `listen_addresses`: Add your server's ip address here.
- `max_connections`: Change this to some reasonable value (we: 10)
- `shared_buffers`: Use something a lot larger than the default. (2GB)
- `work_mem`: (512MB)
- `effective_cache_size`: Adjust according to amount of main memory available.

You will also need to adjust the `pg_hba.conf` file. This is how ours looks
(1.2.3.4 is the web server's ip address):

	# TYPE  DATABASE    USER        CIDR-ADDRESS          METHOD
	host    all     all     1.2.3.4/24         md5
	local   all         postgres                          ident
	local   all         exomate                           md5
	local   all         all                               ident
	host    all         all         127.0.0.1/32          md5
	host    all         all         ::1/128               md5




### Creating tables in the database

To create all tables in the database, run

	exomate-initdb

If you must, you can also *delete everything* and re-create the tables with:

	exomate-initdb --drop


### Annotating variants

As soon as the VCF files have been imported into the database,
`exomate-variantannotate` needs to be run manually.


Third part (exomate-web): Exomate web frontend
----------------------------------------------

The exomate web frontend uses Flask and SQLalchemy on Python 3.3+.


### Requirements

On the web server, the following packages need to be installed.

- Python 3.3+
- PostgreSQL client
- SQLalchemy
- [Flask](http://flask.pocoo.org/docs/installation/)

In Ubuntu

	apt-get install flask flask-admin sqlalchemy psycopg2


The Python WSGI application is served by Apache from /home2/exom/exomate.
Automatic reloading of the Python modules is enabled, but you will need to
touch the file /home2/exom/exomate/exomate.wsgi to trigger it.
See: http://code.google.com/p/modwsgi/wiki/ReloadingSourceCode

Running ./sync.sh in this directory will touch the file for you.


Installation
============



TODO
----

- document Apache-Install/-konfig
- how to WSGI für Flask aufsetzen

TODO move this into its own VirtualHost

    WSGIDaemonProcess exomate # uses by default 1 process and 15 threads
    WSGIScriptAlias /exomate /var/www/exomate/exomate.wsgi
    WSGIProcessGroup exomate
    WSGIApplicationGroup %{GLOBAL}
    # (TODO: is this used?) SetEnv EXOMATE_CONFIG /var/www/exomate.conf
    
    <Directory /var/www/exomate/>
        AuthType basic
        AuthName "exomate"
        AuthUserFile /var/www/exomate/.htpasswd
        Require valid-user
        Order deny,allow
        Allow from all
    </Directory>
    LogLevel info
    Alias /exomate/bam "/vol/exomate/bams"
    <Directory /vol/exomate/bams/>
        Options Indexes
        Order deny,allow
        Allow from all
        AuthType basic
        AuthName "exomate BAM files"
        AuthUserFile /var/www/exomate/.htpasswd
        Require valid-user
        AllowOverride None
    </Directory>


Preparations
============



Additional steps
----------------

Some steps are not automated, yet.

Manually copy and unpack the file $REPO/annotation-roche/roche-exome-hg19.gff.gz
to $EXOME/db/tracks/roche-exome-hg19.gff

Also copy hg19-refseq-refgene.exons.gtf to $EXOME/db/tracks/
# TODO! How did we create this?


Import the SIFT database
------------------------

Convert downloaded SIFT database to a single SQLite database (will have 24 GB):
../scripts/siftdb2one /home2/gigant/exom/siftdb/Human_db_37_ensembl_63/ hg37.ensembl63.sdb

Then import that file into the 'real' database:
cat importsift.sql | sqlite3 hg37.ensembl63.sdb | psql -v ON_ERROR_STOP=1 -q -U exomate

When debugging, run this to clear all tables in the correct order:
echo 'drop table sift cascade;' | psql -U exomate



Finishing
---------

echo 'vacuum; analyze;' | psql -U exomate



### Install exomate scripts

Install the exomate Python module and command-line scripts with:

	python3 setup.py install

This will install the scripts to `/usr/local/bin/`. You can choose a
different path with `--prefix=`... or use a virtualenv. In any case,
the `exomate-`* scripts must be available in the PATH when the pipeline
is run.

The scripts for the web server need to be installed separately.


## Misc

Installing PostgreSQL as a regular user (if you don't have root access):

* http://blog.endpoint.com/2013/06/installing-postgresql-without-root.html
* [PostgreSQL Environment Generator](https://github.com/gregs1104/peg)