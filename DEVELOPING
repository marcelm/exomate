Exomate - Developing
====================

To get started developing with exomate, it is simplest if you have
access to a ready-made database.


Creating a configuration file
-----------------------------

You first need to know the username, password and hostname for
accessing your PostgreSQL database.

Put this in a file named `$HOME/.config/exomate.conf`:

    [database]
    path=postgresql://username:password@hostname/database

"path" is the SQLalchemy connection string, as described at
http://docs.sqlalchemy.org/en/rel_0_7/core/engines.html and
http://docs.sqlalchemy.org/en/rel_0_7/core/engines.html#sqlalchemy.create_engine

Replace username, password, hostname and database with the appropriate
values. If the database name is the same as the user name, you can leave
it out entirely. If you are on Windows, you can find out where your $HOME
directory is by typing this on the Python prompt:

    import os
    print(os.path.expanduser("~"))

Also, you can put the configuration file anywhere you want, but then
you must set the environment variable EXOMATE_CONFIG to the correct
file name. For example:

    export EXOMATE_CONFIG=/home/user/exomate/exomate.conf

The places where exomate looks for the configuration file are, in
order:

	* the configuration file specified by the EXOMATE_CONFIG
		environment variable.
	* $XDG_CONFIG_HOME/exomate.conf
	* ~/.config/exomate.conf

See also exomate/configuration.py.


Starting exomate
----------------

When the configuration file is in place, you can start Exomate with
this command:

    ./run.py

When you open a web browser and go to http://localhost:8000/ , you
should then see the web interface.


Python 2 vs 3
-------------

The aim is for Exomate to be entirely in Python 3 and most scripts
are in Python 3 already. The problem is that Flask is not
available for Python 3, so everything that relates to the web
interface is still in Python 2.

In order to reduce confusion regarding the print statement when
programming, we use the newer print syntax in all files. In order
to get the new syntax in Python 2 code, put this into the header (as
the first statement):

    from __future__ import print_function, division, absolute_import


Debugging SQLalchemy
--------------------

If you want to interactively explore the database contents with
SQLalchemy, you need to instruct SQLalchemy to start a session, which
requires a bit of typing. A script is provided that makes this a bit
easier. Simply run this:

    python -i shell.py

Or better:

	python3 -i shell.py

You will then be in an interactive session with an already
established connection to the database.

You could try this to check if it works:

    transcripts = session.query(Transcript).\
    filter(Transcript.name == 'CHERP-001').all()


IPython
-------

IPython is an enhanced, interactive Python shell.
You can get it from http://ipython.org/ . It comes, for example, with
built-in tab completion (start typing a word, then hit tab to
autocomplete). We recommend trying to use ipython for interactive
work instead of the regular python command. The above command would
become:

	ipython -i shell.py


Policies
--------

The coding guidelines and policies are currently maintained in the wiki.


Unit and functional tests
-------------------------

Unit tests are in the tests/ directory. Run them with

	nosetests -v

Potentially long-running functional tests are in the checks/ directory.
Run them with

	nosetests -v checks
