

CREATE TEMPORARY VIEW patientsinfo AS SELECT patients.accession patient_accession, patients.condition, patients.id, samples.accession sample_accession, tissue, capturekit_id, units.prefix FROM units JOIN samples ON units.sample_id = samples.id JOIN patients ON samples.patient_id = patients.id;


condition = 'cs'
condition = 'none' OR (condition = 'am' AND tissue = 'B') OR (condition = 'rb' AND tissue = 'B')

CREATE TEMPORARY VIEW tmp_affected_rgs AS SELECT prefix FROM patientsinfo WHERE ${AFFECTED};

-- filter out SNPs found in healthy individuals
CREATE TEMPORARY VIEW tmp_unaffected_rgs AS SELECT prefix FROM patientsinfo WHERE ${UNAFFECTED};


# unbedingt hier hin: ein ANALYZE tmp_affected_rgs

CREATE TEMPORARY VIEW tmp_affected_calls AS SELECT * FROM calls WHERE rg in (SELECT rg FROM tmp_affected_rgs);

CREATE TEMPORARY VIEW affected_variants AS SELECT DISTINCT variant_id FROM tmp_affected_calls;



from exomate.database import get_session
session = get_session()
from exomate.models import *
from sqlalchemy import or_, and_

all_units = session.query(Unit.id).join(Sample).join(Patient)
affected_units = all_units.filter(Patient.condition == 'cs').subquery()
unaffected_units = all_units.filter(or_(Patient.condition == 'none', and_(Patient.condition == 'am', Sample.tissue == 'B'), and_(Patient.condition == 'rb', Sample.tissue == 'B'))).subquery()
affected_calls = session.query(Call.variant_id, Call.unit, Call.qual).join(affected_units, affected_units.c.id == Call.c.unit_id).subquery()



print session.query(Call).join(affected_units, Call.afected_units.c)

-- SEE file coffinsirisquery.sql

-- TODO phenoscore is ignored

-- for debugging, this may be useful
-- SELECT array_agg(DISTINCT transcripts.name), transcript_id, count(transcript_id), count(DISTINCT tmp_affected_calls.rg) as n, array_agg(tmp_affected_calls.variant_id) variant_ids, array_agg(DISTINCT tmp_affected_calls.rg) prefixes FROM interest, tmp_affected_calls, annotations, transcripts WHERE transcripts.id = transcript_id AND tmp_affected_calls.variant_id = interest.variant_id AND tmp_affected_calls.variant_id = annotations.variant_id GROUP BY transcript_id HAVING count(DISTINCT tmp_affected_calls.rg) >= 3 ORDER BY n;

