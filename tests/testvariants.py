from exomate.variants import normalized_indel

def testnormalized_str():
	sequence = "ACTGGGCTATACATAGATAGATGATAGCAC"
	ni = normalized_indel
	assert ni(sequence, 5, "GCTAT", "GCT") == (6, "CTA", "C")
	assert ni(sequence, 0, "AC", "AT") == (1, "C", "T")
	assert ni(sequence, 1, "C", "T") == (1, "C", "T")
	assert ni(sequence, 2, "TGG", "T") == (2, "TGG", "T")

def testnormalized_bytes():
	ref = b'TT'
	alt = b'T'
	pos = 9
	chromosome = b'AAATACCCATTATTGCTAAC'
	assert chromosome[pos:pos+len(ref)] == ref
	ni = normalized_indel
	ni(chromosome, pos, ref, b"T")
	ni(chromosome, pos, ref, b"TAT")
	ni(chromosome, pos, ref, b"TTA")
	 
