# kate: syntax Python;
#
# Example configuration file for the exomate pipeline.
# Put this file under the name 'pipeline.config' into the exomate
# pipeline directory.
#
# This file is currently included from the Snakefile via an 'include:' directive
# and can therefore contain any valid Python code.
#
# Path names are relative to the pipeline directory.

# This string is written to the PL ("platform") tag in BAM files.
PLATFORM = 'Illumina'

# Path to the FASTA file with the reference.
# A BWA index is expected to be in the same directory.
REF = 'reference/human_g1k_v37.fasta'

# Path to the cache to use for variant effect prediction by the ensembl VEP tool.
# vep has to be available as vep inside your $PATH
VEP_CACHE = '/vol/ref/vep'

# Quality-trimming option for BWA
# set to the empty string '' to disable
BWAQUALTRIM = '-q 10'

# Memory that samtools sort is allowed to use (in bytes)
SORTMEM = int(1E9)

# Base directory in which the raw FASTQ files are located.
# Paths in the units.txt file are relative to this directory.
READS_BASEDIR = 'raw'

# ENSEMBL human genome annotation track in GTF format. 
HGTRACK = 'db/ensembl.gtf'

# Path to dbSNP in VCF format.
DBSNP = 'db/dbsnp.vcf'

# Path to a directory with exome capture track annotation tracks,
# used to check how well exome capture worked.
# Paths in the 'capturekits' table of the SQL database are
# relative to this directory.
CAPTURE_TRACK_DIR = 'db/capturetracks'

# How to call the GATK.
# If you have a key, you can add -et NO_ET etc.
GATK = 'java -Xmx4G -jar /usr/local/share/gatk/GenomeAnalysisTK.jar'

# Which type of BWA to use. Can be either 'aln' (for BWA-backtrack) or 'mem' (for BWA-MEM).
BWA = 'aln'
